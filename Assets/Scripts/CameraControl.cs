﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public Transform target;

	Vector3 velocity = Vector3.zero;

	float camVertExtent;
	float camHorzExtent;


	public float smoothTime = .15f;

	public bool YMaxEnabled = false;
	public float YMaxValue = 0;

	// enable and set the min Y value
	public bool YMinEnabled = false;
	public float YMinValue = 0;

	// enable and set the max X value
	public bool XMaxEnabled = false;
	public float XMaxValue = 0;

	// enable and set the min X value
	public bool XMinEnabled = false;
	public float XMinValue = 0;

	float leftBound;
	float rightBound;
	float bottomBound;
	float topBound;

	void Start(){
		
		camVertExtent = Camera.main.orthographicSize;
		camHorzExtent = Camera.main.aspect * camVertExtent;

		leftBound   = XMinValue + camHorzExtent -0.5f;
		rightBound  = XMaxValue - camHorzExtent + 0.5f;
		bottomBound = YMinValue + camVertExtent -0.5f;
		topBound    = YMaxValue - camVertExtent + 0.5f;
	
	
	}

	void Update()
	{
		// Target position
		Vector3 targetPos = target.position;

		//vertical
		if (YMinEnabled && YMaxEnabled) {
			targetPos.y = Mathf.Clamp (targetPos.y, bottomBound, topBound);
		}

		else if(YMinEnabled)
			targetPos.y = Mathf.Clamp (targetPos.y, bottomBound, targetPos.y);

		else if(YMaxEnabled)
			targetPos.y = Mathf.Clamp (targetPos.y, targetPos.y, topBound);

		//horizontal
		if (XMinEnabled && XMaxEnabled) {
			targetPos.x = Mathf.Clamp (targetPos.x, leftBound, rightBound);
		}

		else if(XMinEnabled)
			targetPos.x = Mathf.Clamp (targetPos.x, leftBound, targetPos.x);

		else if(XMaxEnabled)
			targetPos.x = Mathf.Clamp (targetPos.x, targetPos.x, rightBound);

		targetPos.z = transform.position.z;

		transform.position = Vector3.SmoothDamp (transform.position, targetPos, ref velocity, smoothTime);

	}

}