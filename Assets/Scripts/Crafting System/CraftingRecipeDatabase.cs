﻿using System.Collections;
using UnityEngine;
using LitJson;
using System.Collections.Generic;
using System.IO;

public class CraftingRecipeDatabase : MonoBehaviour
{
    public List<CraftingRecipe> database;
    //rivate JsonData itemData;

    private void Start()
    {
        //itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/CraftingRecipes.json"));
        //ConstructCraftingRecipeDatabase();
        string json = File.ReadAllText(Application.dataPath + "/StreamingAssets/CraftingRecipes.json");

        //Debug.Log("Loaded File : " + json);

        database = JsonMapper.ToObject<List<CraftingRecipe>>(json);

        //Debug.Log("Recepies Count : " + database.Count);

        for (int i=0; i<database.Count; i++)
        {
            //Debug.Log("Crafting Recipe: " + database[i].description);
        }


    }

    public CraftingRecipe FetchCraftingRecipeByID(int id)
    {
        for (int i = 0; i < database.Count; i++)
            if (database[i].id == id)
                return database[i];
        return null;

    }

    /*
    void ConstructCraftingRecipeDatabase()
    {
        for (int i = 0; i < itemData.Count; i++)
        {

            database.Add(new CraftingRecipe((int)itemData[i]["id"], (int [])itemData[i]["materials"], (int)itemData[i]["results"], itemData[i]["description"].ToString());
        }
    }
    */
}

public class Ingredient
{
    public int id { get; set; }

    public int amount { get; set; }

}


public class CraftingRecipe
{
    public int id { get; set; }
    public List<Ingredient> materials { get; set; }
    public List<Ingredient> results { get; set; }
    public string description { get; set; }

    /*
    public CraftingRecipe(int id, int[] materials, int[] results, string description)
    {
        this.ID = id;

        this.Materials = new int[materials.Length];
        this.Materials = materials;

        this.Results = new int[results.Length];
        this.Results = results;

        this.Description = description;

    }

    public CraftingRecipe()
    {
        this.ID = -1;
    }
    */
}

public class CraftedItem
{
    public Item item;
    public int amount;
    public CraftingRecipe recipe;

    public CraftedItem(Item item, int amount, CraftingRecipe recipe)
    {
        this.item = item;
        this.amount = amount;
        this.recipe = recipe;
    }

    /*
    public CraftingRecipe(int id, int[] materials, int[] results, string description)
    {
        this.ID = id;

        this.Materials = new int[materials.Length];
        this.Materials = materials;

        this.Results = new int[results.Length];
        this.Results = results;

        this.Description = description;

    }

    public CraftingRecipe()
    {
        this.ID = -1;
    }
    */
}
