﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CraftedItemData : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    //public Item item;
    //public int amount;
    //public int slot;
    //public int id;
    public CraftedItem craftedItem;
    //public bool DropAway;

    private GameObject inv;
    public CraftingSystem craftingSystem;
    private Tooltip tooltip;
    //private Vector2 offset;

    //GameObject Canvas;
    //GameObject Handler;
    //Requests RequestScript;
    //GameObject Character;



    private void Start()
    {
        inv = GameObject.Find("Inventory");
        tooltip = inv.GetComponent<Tooltip>();
        //Canvas = GameObject.Find("Canvas");
       // Handler = GameObject.Find("Handler");
        //RequestScript = Handler.GetComponent<Requests>();
        //Character = GameObject.Find("Character");
        //DropAway = true;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        craftingSystem = inv.GetComponent<CraftingSystem>();
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            tooltip.Deactivate();
            craftingSystem.CraftItem(craftedItem);
        }     

        //Debug.Log("Right Mouse Button Clicked on: " + name);

    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        tooltip.Activate(craftedItem);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("Crafted OnPointerExit");
        tooltip.Deactivate();
    }
}
