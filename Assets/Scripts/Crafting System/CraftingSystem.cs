﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingSystem : MonoBehaviour
{
    public Dictionary<int, int> ItemCount;
    public GameObject Inventory;
    public InventoryNew InventoryNew;
    public List<GameObject> slots;
    public GameObject craftingSlot;
    public GameObject craftingSlotPanel;
    public List<GameObject> craftingSlots = new List<GameObject>();
    public GameObject craftingItem;
    //public List<Item> craftingItems = new List<Item>();
    public List<CraftedItem> craftedItems = new List<CraftedItem>();
    public List<CraftingRecipe> CraftingRecipes = new List<CraftingRecipe>();
    public ItemDatabase database;
    public GameObject inventoryItem;
    public GameObject Handler;
    public Requests requestScript;
    public GameObject OpenUp;
    public GameObject OpenDown;
    public GameObject recipeTypeToggleGroup;
    int craftingSlotPanelAmount = 30;
    int lastCraftedItemIndexDefault;
    int lastCraftedItemIndex;

    //public CraftingRecipe PlankOakRecipe;
    

    // Start is called before the first frame update
    void Start()
    {
        lastCraftedItemIndexDefault = craftingSlotPanelAmount - 1;
        Inventory = GameObject.Find("Inventory");
        Handler = GameObject.Find("Handler");
        recipeTypeToggleGroup = GameObject.Find("Recipe Type Toggle Group");
        InventoryNew = Inventory.GetComponent<InventoryNew>();
        //CraftingRecipes = Inventory.GetComponent<CraftingRecipeDatabase>().database;
        slots = InventoryNew.slots;
        //craftingSlots = InventoryNew.craftingSlots;
        ItemCount = new Dictionary<int, int>();
        database = InventoryNew.GetComponent<ItemDatabase>();
        CraftingRecipes = Inventory.GetComponent<CraftingRecipeDatabase>().database;
        craftingSlotPanel = GameObject.Find("Crafting Slot Panel").gameObject;

        requestScript = Handler.GetComponent<Requests>();

        lastCraftedItemIndex = lastCraftedItemIndexDefault;

        for (int i = 0; i < craftingSlotPanelAmount; i++)
        {
            //craftingItems.Add(new Item());
            craftingSlots.Add(Instantiate(craftingSlot));
            //craftingSlots[i].GetComponent<Slot>().id = i;
            craftingSlots[i].transform.SetParent(craftingSlotPanel.transform);
            craftingSlots[i].transform.localScale = new Vector3(1f, 1f, 1f);
        }

        InitialDictionaryFill();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i< recipeTypeToggleGroup.transform.childCount; i++)
        {
            Toggle currentToggle = recipeTypeToggleGroup.transform.GetChild(i).gameObject.GetComponent<Toggle>();

            if (currentToggle.isOn) {
                ColorBlock cb = currentToggle.colors;
                cb.normalColor = new Color32(255, 255, 0, 255);
                currentToggle.colors = cb;
            }
            else
            {
                ColorBlock cb = currentToggle.colors;
                cb.normalColor = new Color32(255, 255, 255, 255);
                currentToggle.colors = cb;
            }
        }


        if (Input.GetAxis("Mouse ScrollWheel") < 0 && lastCraftedItemIndex < craftedItems.Count-1)
        {
            lastCraftedItemIndex++;
            Debug.Log("LastCraftedItemIndex: " + lastCraftedItemIndex);
            CheckRecipes();

        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0 && lastCraftedItemIndex > lastCraftedItemIndexDefault)
        {

            lastCraftedItemIndex--;
            Debug.Log("LastCraftedItemIndex: " + lastCraftedItemIndex);
            CheckRecipes();

        }

        //Items.Add("apple", "macintosh");
        //Debug.Log(Items["apple"]);
    }

    public void InitialDictionaryFill()
    {
        slots = InventoryNew.slots;
        ItemCount = new Dictionary<int, int>();
        //Debug.Log("Slots Count: " + slots.Count);
        //for (int i = 0; i < slots.Count; i++)
        for (int i = 0; i < (InventoryNew.quickAccessPanelAmount + InventoryNew.slotPanelAmount); i++)
        {
            //Debug.Log("Searchable Slots Count: " + (InventoryNew.quickAccessPanelAmount + InventoryNew.slotPanelAmount));
            if (slots.Count > 0) { 
                if (slots[i].transform.childCount > 0)
                {
                    ItemData data = slots[i].transform.GetChild(0).GetComponent<ItemData>();

                    if (ItemCount.ContainsKey(data.id))
                    {
                        ItemCount[data.id] += data.amount;
                    }
                    else
                    {
                        ItemCount.Add(data.id, data.amount);
                    }
                }
            }
        }
    }

    /*
    public void ResetCraftingDictionary()
    {

    }
    */

    public void CheckRecipes()
    {
        //Debug.Log("CHECK RECIPES");
        
        for (int i = 0; i<craftingSlots.Count; i++)
        {
            if (craftingSlots[i].transform.childCount > 0) {
                for (int j = 0; j < craftingSlots[i].transform.childCount; j++) {
                    Destroy(craftingSlots[i].transform.GetChild(j).gameObject);
                }
            }
        }


        //CraftingRecipes = Inventory.GetComponent<CraftingRecipeDatabase>().database;


        //int currentSlot = 0;
        
        craftedItems = new List<CraftedItem>();

        CraftingRecipes = Inventory.GetComponent<CraftingRecipeDatabase>().database;

        for (int i=0; i<CraftingRecipes.Count; i++)
        {
            
            bool craft = true;
            for(int j=0; j < CraftingRecipes[i].materials.Count; j++)
            {

                if (ItemCount.ContainsKey(CraftingRecipes[i].materials[j].id)){

                    if (ItemCount[CraftingRecipes[i].materials[j].id] < CraftingRecipes[i].materials[j].amount)
                    {
                        //Debug.Log("WORKED");
                        craft = false;
                        break;
                    }
                }
                else
                {
                    //Debug.Log("WORKED");
                    craft = false;
                    break;
                }
            }

            if (craft == false) {
                continue;
            }
            else
            {
                for (int j = 0; j < CraftingRecipes[i].results.Count; j++) {
                    Item itemToAdd = database.FetchItemById(CraftingRecipes[i].results[j].id);
                    craftedItems.Add(new CraftedItem(itemToAdd, CraftingRecipes[i].results[j].amount, CraftingRecipes[i]));
                   
                    //itemObj.GetComponent<ItemData>().slot = i;


                    
                }
            }

        }


        if (lastCraftedItemIndex > craftedItems.Count - 1)
        {
            if (craftedItems.Count > 0)
            {
                if (craftedItems.Count < lastCraftedItemIndexDefault + 1)
                {
                    lastCraftedItemIndex = lastCraftedItemIndexDefault;
                }
                else
                {
                    lastCraftedItemIndex = craftedItems.Count-1;
                }
            }
            else
            {
                lastCraftedItemIndex = lastCraftedItemIndexDefault;
            }
        }

        

        FillCraftingSlots();





    }

    public void UpdateDictionary(int key, int value)
    {
        if (ItemCount.ContainsKey(key))
        {
            ItemCount[key] += value;
        }
        else
        {
            ItemCount.Add(key, value);
        }

        //PrintOut();
        CheckRecipes();
    }

    void FillCraftingSlots()
    {
        //Debug.Log("FillCraftingSlots");
        //Debug.Log("LastCraftedItemIndex: " + lastCraftedItemIndex);
        //Debug.Log("CraftedItemsCount: " + craftedItems.Count);

        //ArrowUp check
        if (lastCraftedItemIndex > lastCraftedItemIndexDefault)
            OpenUp.SetActive(true);
        else
            OpenUp.SetActive(false);

        //ArrowDown check
        if (lastCraftedItemIndex < craftedItems.Count - 1)
            OpenDown.SetActive(true);
        else
            OpenDown.SetActive(false);

        /*
        if (lastCraftedItemIndex + 1 < craftedItems.Count)
        {
        */
        int slotIndex = 0;
        for (int i = lastCraftedItemIndex- lastCraftedItemIndexDefault; i < craftedItems.Count && i <= lastCraftedItemIndex; i++)
        {
                
                GameObject itemObj = Instantiate(craftingItem);
                itemObj.GetComponent<CraftedItemData>().craftedItem = craftedItems[i];

                itemObj.transform.SetParent(craftingSlots[slotIndex].transform);
                itemObj.transform.localPosition = Vector3.zero;
                itemObj.transform.localScale = new Vector3(1f, 1f, 1f);
                //itemObj.GetComponent<Image>().sprite = itemObj.GetComponent<CraftedItemData>().craftedItem.item.sprite;
                itemObj.GetComponent<Image>().sprite = Handler.GetComponent<Requests>().spritesDictionary[itemObj.GetComponent<CraftedItemData>().craftedItem.item.slug];
                itemObj.name = itemObj.GetComponent<CraftedItemData>().craftedItem.item.title;

                if (itemObj.GetComponent<CraftedItemData>().craftedItem.amount == 1)
                {
                    itemObj.transform.GetChild(0).GetComponent<Text>().text = "";
                }
                else
                {
                    itemObj.transform.GetChild(0).GetComponent<Text>().text = itemObj.GetComponent<CraftedItemData>().craftedItem.amount.ToString();
                }

            slotIndex++;
                //currentSlot++;
        }
        //}
       
    }

    public void ResetLastCraftedItemIndex()
    {
        lastCraftedItemIndex = lastCraftedItemIndexDefault;
        //ArrowUp.SetActive(false);
    }

    /*
    public void PrintOut()
    {
        foreach (KeyValuePair<int, int> pair in ItemCount)
        {
            //Debug.Log("Item: "+ pair.Key +" Count: " + pair.Value);
        }
    }
    */

    public void CraftItem(CraftedItem craftedItem)
    {
        StartCoroutine(Craft(craftedItem));
    }




    public IEnumerator Craft(CraftedItem craftedItem)
    {
        for (int i = 0; i < craftedItem.recipe.materials.Count; i++) {
            InventoryNew.DeleteItems(craftedItem.recipe.materials[i].id, craftedItem.recipe.materials[i].amount);
        }

        yield return null;

        for (int i = 0; i < craftedItem.recipe.results.Count; i++)
        {
            int addedAmount = InventoryNew.AddItems(craftedItem.recipe.results[i].id, craftedItem.recipe.results[i].amount);
            int resultId = craftedItem.recipe.results[i].id;
            if (addedAmount != craftedItem.recipe.results[i].amount)
            {
                int remainder = craftedItem.recipe.results[i].amount - addedAmount;
                requestScript.DropItems(resultId, remainder);
            }

        }

        
    }
    



    /*
        public void CheckCraft()
    {
        bool canCraft = false ;
        //craftingItems = new List<Item>();
         
        //if (craftingSlots[0].transform.childCount > 0)
            //Destroy(craftingSlots[0].transform.GetChild(0));
        /*
        foreach (ItemAmount itemAmount in PlankOakRecipe.Materials)
        {
                if (ItemCount.ContainsKey(itemAmount.id) && ItemCount[itemAmount.id] >= itemAmount.Amount)
                {
                Debug.Log("Item Count.id = " + ItemCount[itemAmount.id]);
                    canCraft = true;
                    break;
                }
            
        }
        
        if (canCraft)
        {
            /*
            Item itemToAdd = database.FetchItemByID(PlankOakRecipe.Results[0].id);
            
            craftingItems.Add(itemToAdd);
            GameObject itemObj = Instantiate(inventoryItem);
            itemObj.GetComponent<ItemData>().item = itemToAdd;
            itemObj.GetComponent<ItemData>().amount = PlankOakRecipe.Results[0].Amount;
            itemObj.GetComponent<ItemData>().slot = 0;


            itemObj.GetComponent<ItemData>().id = PlankOakRecipe.Results[0].id;


            itemObj.transform.SetParent(craftingSlots[0].transform);
            itemObj.transform.localPosition = Vector3.zero;
            itemObj.transform.localScale = new Vector3(1f, 1f, 1f);
            itemObj.GetComponent<Image>().sprite = itemToAdd.Sprite;
            itemObj.transform.GetChild(0).GetComponent<Text>().text = itemObj.GetComponent<ItemData>().amount.ToString();
            itemObj.name = itemToAdd.Title;
            
        }

    }
    */
}
