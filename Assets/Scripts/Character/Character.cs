﻿using UnityEngine;
using System.Collections;
using System;



[RequireComponent(typeof(Rigidbody2D))]
public class Character : MonoBehaviour
{
    public float speed;
	public int destructionPower;
    public Sprite[] walkSprites;
	public Sprite[] jumpSprites;
    public Sprite idleSprite;
    public float jumpSpeed;
    public float fallMultiplier;
    public float lowJumpMultiplier;

    public bool canRaycast;

    public bool IsGrounded;

    //public Transform top_left ;
    //public Transform bottom_right;

	public LayerMask defaultLayer;

	public GameObject MainCamera;
	public GameObject Inventory;
    public InventoryNew InventoryNew;
    public UI_Handler UI_Handler;
	public GameObject Handler;
    public Collider2D GroundCollider;

    //GameObject Chunks;



    RaycastHit2D hitlefthigh;
	RaycastHit2D hitrighthigh;
	RaycastHit2D hitleft;
	RaycastHit2D hitright;




	// World Bounds
	float YMaxValue;

	float YMinValue;

	float XMaxValue;

	float XMinValue;


    public Material AirTexture;

    bool isWalking = false;
	public bool face_right = true;
	bool block_destroyed = true;

    RaycastHit2D hit;
	Rigidbody2D r;
	BoxCollider2D BColl;

	//AutoClimb RayCaster
	Vector2 position_low;
	Vector2 position_high;

	Vector2 direction_right;
	Vector2 direction_left;

	float distance_right;
	float distance_left;

	float Autoclimb_changespeed;

    public CharacterController2D controller;


	void Start(){

        IsGrounded = false;
		canRaycast = true;

		r = GetComponent<Rigidbody2D>();

        InventoryNew = Inventory.GetComponent<InventoryNew>();
        UI_Handler = Handler.GetComponent<UI_Handler>();

        BColl = GetComponent<BoxCollider2D> ();	

		YMaxValue = MainCamera.GetComponent<CameraControl>().YMaxValue;

		YMinValue = MainCamera.GetComponent<CameraControl>().YMinValue + GetComponent<BoxCollider2D>().size.y/2 + 0.5f;

		XMaxValue = MainCamera.GetComponent<CameraControl>().XMaxValue;

		XMinValue = MainCamera.GetComponent<CameraControl>().XMinValue + GetComponent<BoxCollider2D>().size.x/2;

        //Chunks = GameObject.Find("Chunks(Clone)");


    }






    void Update()
    {
        /*

        if (!UI_Handler.IsAnything_Showing())
        {
            // Check world bound for X
            if (transform.position.x <= XMinValue)
            {

                transform.position = new Vector3(XMinValue, transform.position.y, transform.position.z);
            }

            if (transform.position.x >= XMaxValue)
            {

                transform.position = new Vector3(XMaxValue, transform.position.y, transform.position.z);
            }


            // Check world bound for Y
            if (transform.position.y > YMinValue)
            {
                r.gravityScale = 1f;

                r.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, r.velocity.y + 0.01f);

            }
            else
            {
                r.gravityScale = 0f;

                r.velocity = new Vector2(Input.GetAxis("Horizontal") * speed, 0f);

                transform.position = new Vector3(transform.position.x, YMinValue, transform.position.z);
            }

            if (transform.position.y >= YMaxValue)
            {

                transform.position = new Vector3(transform.position.x, YMaxValue, transform.position.z);
            }


            // Chack for face orientation
            if (Input.GetAxisRaw("Horizontal") < 0 && face_right)
            {
                transform.localScale = new Vector3(-2.5f, 2.5f, 1);
                face_right = false;
            }
            else if (Input.GetAxis("Horizontal") > 0 && !face_right)
            {
                transform.localScale = new Vector3(2.5f, 2.5f, 1);
                face_right = true;
            }

        */

        // AutoClimb System
        /*

        position_low = new Vector2 (BColl.bounds.center.x, BColl.bounds.center.y - 1.2f);
        position_high = new Vector2 (BColl.bounds.center.x, BColl.bounds.center.y);

        direction_right = Vector2.right;
        direction_left = Vector2.left;

        distance_right = BColl.size.x + 0.35f;
        distance_left = BColl.size.x + 0.20f;

        Autoclimb_changespeed = 0.575f;

        if (face_right) {

            hitrighthigh = Physics2D.Raycast (position_high, direction_right, distance_right, defaultLayer);
            hitright = Physics2D.Raycast (position_low, direction_right, distance_right, defaultLayer);

            Debug.DrawRay (position_high, direction_right * distance_right, Color.green);
            Debug.DrawRay (position_low, direction_right * distance_right, Color.green);

            if (hitrighthigh.collider == null) {
                if (hitright.collider != null && Input.GetAxisRaw ("Horizontal") > 0f && !hitright.collider.gameObject.name.Contains("Dropped Item")) {

                    transform.position = Vector3.Lerp (transform.position, new Vector3 (transform.position.x,
                        hitright.collider.gameObject.transform.position.y + 2.3f, transform.position.z), Autoclimb_changespeed);
                }

            }


        } else if (!face_right) {
            hitlefthigh = Physics2D.Raycast (position_high, direction_left, distance_right, defaultLayer);
            hitleft = Physics2D.Raycast (position_low, direction_left, distance_right, defaultLayer);

            Debug.DrawRay (position_high, direction_left * distance_right, Color.white);
            Debug.DrawRay (position_low, direction_left * distance_right, Color.white);

            if (hitlefthigh.collider == null) {
                if (hitleft.collider != null && Input.GetAxisRaw ("Horizontal") < 0f && !hitleft.collider.gameObject.name.Contains("Dropped Item")) {

                    transform.position = Vector3.Lerp (transform.position, new Vector3 (transform.position.x,
                        hitleft.collider.gameObject.transform.position.y + 2.3f, transform.position.z), Autoclimb_changespeed);
                }

            }
        }

*/

        /*

            // Walk, Jump and Idle states

            if (IsGrounded)
            {

                BColl.offset = new Vector2(0.0296f, -0.0666f);
                if (Input.GetAxisRaw("Horizontal") != 0f && Input.GetAxis("Jump") == 0)
                {
                    if (!isWalking)
                    {
                        StartCoroutine(Walk());
                    }
                }
                else if (Input.GetAxis("Jump") > 0)
                {
                    //r.velocity = new Vector2 (r.velocity.x, jumpSpeed);
                    StartCoroutine(Jump());

                }
                else if (Input.GetAxis("Jump") > 0 && Input.GetAxisRaw("Horizontal") != 0f)
                {
                    //r.velocity = new Vector2 (r.velocity.x, jumpSpeed);
                    StartCoroutine(Jump());

                }
                else if (r.velocity.magnitude < 0.1f && Input.GetAxis("Jump") == 0 && Input.GetAxisRaw("Horizontal") == 0)
                {
                    GetComponent<SpriteRenderer>().sprite = idleSprite;

                }
            }

            //Better jumping script
            if (r.velocity.y < 0)
            {
                r.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }

        */
       
        // Block destruction ("Fire1" = Left mouse button)


        if (Input.GetAxisRaw("Fire1") == 1 && block_destroyed == true && !UI_Handler.IsAnything_Showing() && canRaycast)
            {
                //get mouse position
                Vector3 c = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                //raycast to mouse position
                RaycastHit2D hit2d = Physics2D.Raycast(transform.position, c - transform.position);

                //if something was hit
                if (hit2d == true)
                {
                    //hit object
                    GameObject hit_tile = hit2d.collider.gameObject;
                    //distance to object
                    float distance = Vector3.Distance(transform.position, hit_tile.transform.position);

                    //if it was not dropped item and distance to it was <= 5f
                    if (!hit_tile.name.Contains("Dropped Item") && distance <= 5)
                    {
                        block_destroyed = false;
                        StartCoroutine(Destroy_Block(hit_tile));
                    }

                }
            }
            

            // Block placement ("Fire2" = Right mouse button)

            if (Input.GetAxis("Fire2") > 0 && block_destroyed == true && !UI_Handler.IsAnything_Showing())
            {


                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                Vector3 placePos = new Vector3(Mathf.Round(mousePos.x), Mathf.Round(mousePos.y), 0f);

                var distance = Vector3.Distance(transform.position, placePos);

                ItemData selected_item = InventoryNew.selectedItem;






                if (distance <= 5)
                {

                    GameObject clickedTile = Handler.GetComponent<Requests>().GetTile(placePos);

                    if (Physics2D.OverlapCircleAll(placePos, 0.25f).Length == 0 && InventoryNew.selectedItem != null)
                    {
                        InventoryNew.CheckForSelectedTile(InventoryNew.selectedSlot);
                        Debug.Log("Name: " + selected_item.name);

                        //Handler.GetComponent<Requests>().DestroyTile(placePos);

                        Handler.GetComponent<Requests>().PlaceTile(placePos, selected_item.name);

                        InventoryNew.DeleteItem(selected_item.id, selected_item.slot);
                    }



                    else if (clickedTile.tag == "Complex" && clickedTile.name.Contains("Furnace"))
                    {
                        Debug.Log("Furnace");
                        if(!clickedTile.GetComponent<Furnace>().recentlyPlaced)
                            clickedTile.GetComponent<Furnace>().showFurnacePanel();
                    }


                    Debug.Log("Distance: " + distance);












                    /*
                    GameObject newTile;

                    if (InventoryNew.selectedItem.id < 500) {
						newTile = Instantiate (Resources.Load<GameObject> ("Tiles/Basic Tiles/" + selected_item.name), placePos, Quaternion.identity) as GameObject;
					} else {
						newTile = Instantiate (Resources.Load<GameObject> ("Tiles/Ores/" + selected_item.name), placePos, Quaternion.identity) as GameObject;
					}

                    int chunk_height = Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().height;
                    int chunk_width = Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().width;
                    int chunk_number = Mathf.FloorToInt((int)placePos.x / chunk_width);
                    //Debug.Log("CHUNKI NUMBI: " + chunk_number);
                    int tile_hierarchy_place = (chunk_height * (int)newTile.transform.position.x) +(int)newTile.transform.position.y;

                    Destroy(Chunks.transform.GetChild(chunk_number).GetChild(tile_hierarchy_place).gameObject);

                    // Adding new block to a chunk
                    newTile.transform.SetParent(Chunks.transform.GetChild(chunk_number));

                    //newTile.transform.SetSiblingIndex(0);
                    newTile.transform.SetSiblingIndex(tile_hierarchy_place);
                    //Debug.Log("IMPORTANT: "+ (((int)Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().height * (int)newTile.transform.position.x) + (int)newTile.transform.position.x));
                    InventoryNew.DeleteItem(selected_item.id, selected_item.slot);

					//Inventory.GetComponent<InventoryNew> ().UpdateQuickPanelDown ();
					InventoryNew.CheckForSelectedTile (InventoryNew.selectedSlot);

                    */


                }
            }
        }
       

        //}

    private void FixedUpdate()
    {
        
    }


    public void OnDrawGizmos()
    {
        /*
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        //Gizmos.DrawCube(this.transform.position - new Vector3(0f, (float)this.transform.localScale.y / 2 - 0.5f), new Vector2(0.25f, 0.25f));
        Gizmos.DrawCube(this.transform.position - new Vector3(0f, (float)this.transform.localScale.y - 0.7f), new Vector2(0.4f, 0.4f));

        Gizmos.color = new Color(0, 1, 0, 0.5f);
        Gizmos.DrawSphere(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.1f);

        Gizmos.color = new Color(0, 0, 1, 0.5f);
        Gizmos.DrawLine(this.transform.position, Camera.main.ScreenToWorldPoint(Input.mousePosition));
        */

    }


    void OnCollisionEnter2D(Collision2D col)
    {
        //!col.gameObject.name.Contains("Dropped Item")
        //Debug.Log("RANNNNNNN!");
        if (col.gameObject.name != "Character" && !col.gameObject.name.Contains("Dropped Item"))
        {
            if(Physics2D.OverlapBox(this.transform.position - new Vector3(0f, (float)this.transform.localScale.y - 0.7f), new Vector2(0.4f, 0.4f), 0f) != null)
                IsGrounded = true;
        }
            

       // if (col.gameObject.name.Contains("Dropped Item"))
        //    StartCoroutine(MoveToPosition(col.gameObject.transform, col.gameObject, 0.5f));

    }

    /*
    void OnTriggerStay2D(Collider2D col)
    {
        Debug.Log("OnTriggerStay");
        //Debug.Log(col.gameObject.name + " : " + gameObject.name + " : " + Time.time);
        if (col.gameObject.name.Contains("Dropped Item"))
        {

            if (col.gameObject.GetComponent<DroppedItemData>().isMoving == false)
            {
                col.gameObject.GetComponent<DroppedItemData>().isMoving = true;
               
                StartCoroutine(MoveAndAddItem(this.transform, col.gameObject, 0.3f));
            }
        }


    }
    */

    void OnTriggerStay2D(Collider2D col)
    {
        //Debug.Log("OnTriggerEnter");
        //Debug.Log(col.gameObject.name + " : " + gameObject.name + " : " + Time.time);
        if (col.gameObject.name.Contains("Dropped Item"))
        {
            //Debug.Log("OnTrigger Dropped Item");
            if (col.gameObject.GetComponent<DroppedItemData>().isMoving == false)
            {
                //Debug.Log("OnTrigger Dropped Item isNotMoving");
                
                
                StartCoroutine(MoveAndAddItem(this.transform, col.gameObject, 0.3f));
            }
        }


    }






    public IEnumerator MoveAndAddItem(Transform character, GameObject object_to_move, float timeToMove)
    {
        //Debug.Log("Yeah2");
        var currentPos = object_to_move.transform.position;
        var t = 0f;
        //Debug.Log("Can Add?: " + InventoryNew.canAdd(object_to_move.GetComponent<DroppedItemData>().id));

        int slotToAdd = InventoryNew.canAdd(object_to_move.GetComponent<DroppedItemData>().id);

        if (slotToAdd != -1)
        {
            object_to_move.gameObject.GetComponent<DroppedItemData>().isMoving = true;

            object_to_move.GetComponent<DroppedItemData>().innerCollider.enabled = false;
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                object_to_move.transform.position = Vector3.Lerp(currentPos, character.position, t);
                yield return null;
            }
            int addedItemAmount = InventoryNew.AddItems(object_to_move.GetComponent<DroppedItemData>().id, object_to_move.GetComponent<DroppedItemData>().amount);

            object_to_move.gameObject.GetComponent<DroppedItemData>().amount -= addedItemAmount;

            if (object_to_move.gameObject.GetComponent<DroppedItemData>().amount == 0)
                Destroy(object_to_move.gameObject);
            else
            {
                object_to_move.GetComponent<DroppedItemData>().innerCollider.enabled = true;
                object_to_move.GetComponent<DroppedItemData>().isMoving = false;
            }
        }
        else
        {
            object_to_move.GetComponent<DroppedItemData>().isMoving = false;
        }
    }
    


    IEnumerator Walk()
    {
		Rigidbody2D r = GetComponent<Rigidbody2D>();

		isWalking = true;
		for (int l = 0; l < walkSprites.Length; l++) {
            if (Input.GetAxis("Jump") == 0f && r.velocity.magnitude > 0.1f)
            {
                GetComponent<SpriteRenderer>().sprite = walkSprites[l];
                yield return new WaitForSeconds(1 / 30f);
            }
            else
                break;
            
		}
		isWalking = false;
	}

	IEnumerator Jump()
	{


        isWalking = false;
        IsGrounded = false;

        r.velocity = new Vector2(r.velocity.x, jumpSpeed);

        for (int l = 0; l < 11; l++) {
			if(BColl.offset.y <0.1028f){
				BColl.offset += new Vector2 (0f, 0.0154f);
			}
				
				GetComponent<SpriteRenderer> ().sprite = jumpSprites [l];
				yield return new WaitForSeconds (1 / 22f);
			}

	}


    IEnumerator Destroy_Block(GameObject hittile)
	{
        float t = 0f;
        float timeToDestroy = (float)hittile.GetComponent<TileData>().Strength / (float)destructionPower;
        //Debug.Log("TIME TO DESTROY: " + timeToDestroy);

        if(hittile.GetComponent<SpriteRenderer>() != null)
            hittile.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 115);

        while (t < 1)
        {
            if (Input.GetAxis("Fire1") > 0)
            {
                t += Time.deltaTime / timeToDestroy;
                yield return null;
            }

            else
            {
                block_destroyed = true;
                break;
            }
        }

        if (block_destroyed == true)
        {
            if (hittile.GetComponent<SpriteRenderer>() != null)
                hittile.GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
            yield break;
        }

        int destroyed_tile_id = hittile.GetComponent<TileData>().id;
        Vector3 destroyed_tile_position = hittile.transform.position;
        int dirt_tile_id = 2;

        //if it is grass
        if (destroyed_tile_id == 1)
        {
            Handler.GetComponent<Requests>().PlaceItem(hittile.transform, dirt_tile_id);
            Handler.GetComponent<Requests>().PlaceTile(destroyed_tile_position, "Air Tile");
        }

        //if it is complex object (e.g. Tree, Furnace ...)
        else if (hittile.tag == "Complex")
        {
            if (!hittile.name.Contains("Furnace"))
                hittile.transform.parent.GetComponent<TreeScript>().Collapse(hittile.transform.GetSiblingIndex());
            else
            {
                Handler.GetComponent<Requests>().PlaceItem(hittile.transform, destroyed_tile_id);
                Handler.GetComponent<Requests>().PlaceTile(destroyed_tile_position, "Air Tile");
            }

        }
            

        else
        {
            Handler.GetComponent<Requests>().PlaceItem(hittile.transform, destroyed_tile_id);
            Handler.GetComponent<Requests>().PlaceTile(destroyed_tile_position, "Air Tile");
        }

        //Destroy (hittile);


        


        /*
        //Place Air Tile in place of destroyed tile
        GameObject newTile = Instantiate(Resources.Load<GameObject>("Tiles/Basic Tiles/Air Tile"), destroyed_tile_transform.position, Quaternion.identity) as GameObject;

        // Adding new block to a chunk
        newTile.transform.SetParent(Chunks.transform.GetChild(Mathf.FloorToInt(newTile.transform.position.x / Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().width)));

        //Placing in right place in hierarchy
        newTile.transform.SetSiblingIndex((Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().height * (int)newTile.transform.position.x) + (int)newTile.transform.position.y);
        */

        block_destroyed = true;



        yield break;
	}


    // Check if character is on the ground
    /*
	bool IsGrounded() {
		Vector2 position = new Vector2(BColl.bounds.center.x, BColl.bounds.center.y);
		Vector2 direction = Vector2.down;
		float distance = transform.localScale.y * 0.60f;

		Debug.DrawRay (position,direction*distance, Color.red);

		RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, defaultLayer);
		if (hit.collider != null || transform.position.y == YMinValue) {
		

			return true;
		}
		return false;
	}
		*/

}
