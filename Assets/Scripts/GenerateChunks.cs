﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateChunks : MonoBehaviour {

    public GameObject chunk;
    int chunkWidth;
    public int numChunks;
    float seed;
	public GameObject chunks;
    public GameObject newChunk;

    // Use this for initialization
    void Start () {
        chunkWidth = chunk.GetComponent<GenerateChunk>().width;
        seed = Random.Range(-10000f, 10000f);
        Generate();
	}

	public void Generate () {

		// Create Chunks object
		GameObject Chunks = (GameObject)Instantiate(chunks, Vector3.zero, Quaternion.identity) as GameObject;
        int lastX = -chunkWidth;
        for (int i = 0; i < numChunks; i++)
        {
            // Create Chunk object
            newChunk = Instantiate(chunk, new Vector3(lastX + chunkWidth, 0f), Quaternion.identity) as GameObject;

            // Making Chunk child of Chunks
            newChunk.transform.parent = Chunks.transform;
            newChunk.GetComponent<GenerateChunk>().seed = seed;
            lastX += chunkWidth;
        }
	}
}
