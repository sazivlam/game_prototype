﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeScript : MonoBehaviour
{
    //public GameObject Handler;
    public GameObject[,] World_GameObjects;

    public int tree_height;
    public int leaves_side_size;
    public int leaves_bot_pos;
    public int leaves_top_pos;
    public int chunk_width;

    public GameObject LogOakTile;
    public GameObject LeavesOakTile;
    public GameObject Handler;
    public Requests RequestScript;
    public GameObject PlacedTile;
    public GameObject Chunk;

    private Vector3 NewLocalPosition;
    private Vector3 NewPosition;

    GenerateChunk Script;

    // Start is called before the first frame update
    void Start()
    {
        Chunk = this.transform.parent.gameObject;
        chunk_width = Chunk.GetComponent<GenerateChunk>().width;
        Script = this.transform.parent.GetComponent<GenerateChunk>();
        Handler = GameObject.Find("Handler");
        RequestScript = Handler.GetComponent<Requests>();
        World_GameObjects = Script.World_GameObjects;

        tree_height = Random.Range(5, 9);
        leaves_side_size = 2;
        leaves_bot_pos = Mathf.FloorToInt((float)tree_height / 2);
        leaves_top_pos = tree_height + leaves_bot_pos;

        //Replace Grass Tile with Dirt Tile
        PlacedTile = RequestScript.PlaceTile(this.transform.position - new Vector3(0f, 1f), "Dirt Tile");

        //add it to tree
        PlacedTile.transform.parent = this.gameObject.transform;

        //Set tag Complex
        PlacedTile.tag = "Complex";

        for (int y = 0; y < leaves_top_pos; y++)
        {


            NewLocalPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y + y, 0f);
            NewPosition = new Vector3(this.transform.position.x, this.transform.position.y + y, 0f);

            if (RequestScript.GetTile(NewPosition).name == "Air Tile(Clone)" || RequestScript.GetTile(NewPosition).name == "Leaves Oak Tile(Clone) ")
            {

                if (NewLocalPosition.x >= 0 && NewLocalPosition.y >= 0 && NewLocalPosition.x < Script.width && NewLocalPosition.y < Script.height)
                {
                    if (y < tree_height)
                    {
                        //Place Log Oak tiles and place it in passthrough layer
                        PlacedTile = RequestScript.PlaceTile(NewPosition, "Log Oak Tile", 9);

                        
                        if (y >= leaves_bot_pos)
                        {

                            //Debug.Log("NAME: " + addedItem.Title.Replace("Tile", ""));
                            
                            /*
                            Material[] mats = new Material[2];
                            mats[0] = Resources.Load<Material>("Materials/Basics/" + "Log Oak Tile");
                            mats[1] = Resources.Load<Material>("Materials/Basics/" + "Leaves Oak Tile");

                            PlacedTile.GetComponent<SpriteRenderer>().materials = mats;
                            */

                            PlacedTile.GetComponent<SpriteRenderer>().sprite = RequestScript.spritesDictionary["log_oak_leaves_oak"];
                        }
                        


                        //Make Tree a parent
                        PlacedTile.transform.parent = this.gameObject.transform;
                        //Set tag Complex
                        PlacedTile.tag = "Complex";
                    }


                    if (y >= leaves_bot_pos)
                    {
                        for (int x = -leaves_side_size; x <= leaves_side_size; x++)
                        {

                            if (x != 0 || (x == 0 && y >= tree_height))
                            {
                                //Check if it is not outside the world and that it does not replace other non Air or Leaves tiles
                                if (NewLocalPosition.x + x < chunk_width && NewLocalPosition.x + x >= 0) //127 - current width of the world
                                {
                                    //Debug.Log("NewPosition.x+x = " + (NewPosition.x + x));
                                    //Place Leaves Oak tiles
                                    PlacedTile = RequestScript.PlaceTile(NewPosition + new Vector3(x, 0f), "Leaves Oak Tile", 9);
                                    //Make Tree a parent
                                    PlacedTile.transform.parent = this.gameObject.transform;
                                }

                            }


                            if (x == 0 && y >= tree_height)
                            {
                                //Place Leaves Oak tiles
                                PlacedTile = RequestScript.PlaceTile(NewPosition + new Vector3(x, 0f), "Leaves Oak Tile", 9);
                                //Make Tree a parent
                                PlacedTile.transform.parent = this.gameObject.transform;
                            }


                        }

                    }

                }

            }
            else
            {
                break;
            }
        }
    }

    public void Collapse(int index)
    {
        GameObject TileToDestroy;

        for (int i = index; i<this.transform.childCount; i++)
        {
            TileToDestroy = this.transform.GetChild(i).gameObject;

            RequestScript.PlaceItem(TileToDestroy.transform, TileToDestroy.GetComponent<TileData>().id);
            RequestScript.PlaceTile(TileToDestroy.transform.position, "Air Tile");
        }

        if (index == 0)
        {
            Destroy(this.gameObject);
        }

        if(index == 1)
        {
            TileToDestroy = this.transform.GetChild(index-1).gameObject;
            TileToDestroy.tag = "Untagged";
            TileToDestroy.transform.parent = this.gameObject.transform.parent;
            Destroy(this.gameObject);

        }

        //yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
