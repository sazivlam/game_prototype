﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {
    //public Item item;
    private string data;
    private GameObject tooltip;
    public GameObject RecipePanel;

    public GameObject craftingSlot;
    public GameObject craftedItem;
    public GameObject arrow;

    public GameObject Handler;

    ItemDatabase database;

    private void Start()
    {
        tooltip = GameObject.Find("Tooltip");
        //RecipePanel = GameObject.Find("Recipe Panel");
        database = GameObject.Find("Inventory").GetComponent<ItemDatabase>();
        Handler = GameObject.Find("Handler");
        tooltip.SetActive(false);
    }

    private void Update()
    {
        if (tooltip.activeSelf)
        {
            if (RecipePanel.activeSelf)
                tooltip.transform.position = Input.mousePosition;            
            else
                tooltip.transform.position = Input.mousePosition + 
                    new Vector3(0f, + (tooltip.GetComponent<RectTransform>().sizeDelta.y / 1.4f * tooltip.GetComponent<RectTransform>().lossyScale.y));
        }
    }

    public void Activate(Item item)
    {
        //this.item = item;
        ConstructDataString(item);
        tooltip.SetActive(true);
    }

    public void Activate(CraftedItem CraftedItem)
    {
        //this.item = CraftedItem.item;
        ConstructRecipe(CraftedItem);
        tooltip.SetActive(true);
    }

    public void Deactivate()
    {
        RecipePanel.SetActive(false);
        tooltip.SetActive(false);        
    }

    public void ConstructDataString(Item item)
    {
        //data = "<color=#0473f0><b>" + item.title + "</b></color>\n\n" + item.description + "\nPower: " + item.stats.power;
        data = "<color=white><b>" + item.title + "</b></color>\n\n" + item.description; //+ "\nPower: " + item.stats.power;
        tooltip.transform.GetChild(0).GetComponent<Text>().text = data;
    }

    public void ConstructRecipe(CraftedItem CraftedItem)
    {
        data = "<color=white><b>" + CraftedItem.item.title + "</b></color>";//\n\n" + CraftedItem.recipe.description;
        tooltip.transform.GetChild(0).GetComponent<Text>().text = data;




        for (int i = 0; i < RecipePanel.transform.childCount; i++)
        {
            if (RecipePanel.transform.childCount > 0)
                    Destroy(RecipePanel.transform.GetChild(i).gameObject);
       
        }




        // Instantiate slots and materials
        for (int i = 0; i < CraftedItem.recipe.materials.Count; i++)
        {
            GameObject newSlot = Instantiate(craftingSlot);
            newSlot.transform.SetParent(RecipePanel.transform);
            newSlot.transform.localScale = new Vector3(1f, 1f, 1f);

            GameObject itemObj = Instantiate(craftedItem);
            itemObj.transform.SetParent(newSlot.transform);
            itemObj.transform.localPosition = Vector3.zero;
            itemObj.transform.localScale = new Vector3(1f, 1f, 1f);
            //itemObj.GetComponent<Image>().sprite = database.FetchItemById(CraftedItem.recipe.materials[i].id).sprite;////////////////////////////////////////////////////////////////////////////
            itemObj.GetComponent<Image>().sprite = Handler.GetComponent<Requests>().spritesDictionary[database.FetchItemById(CraftedItem.recipe.materials[i].id).slug];
            itemObj.GetComponent<CraftedItemData>().craftedItem = new CraftedItem(database.FetchItemById(CraftedItem.recipe.materials[i].id), CraftedItem.recipe.materials[i].amount, CraftedItem.recipe);

            //itemObj.name = itemObj.GetComponent<CraftedItemData>().craftedItem.item.Title;

            
            if (itemObj.GetComponent<CraftedItemData>().craftedItem.amount == 1)
            {
                itemObj.transform.GetChild(0).GetComponent<Text>().text = "";
            }
            else
            {
                itemObj.transform.GetChild(0).GetComponent<Text>().text = itemObj.GetComponent<CraftedItemData>().craftedItem.amount.ToString();
            }
            
        
        }

        //Instantiate arrow
        GameObject newArrow = Instantiate(arrow);
        newArrow.transform.SetParent(RecipePanel.transform);
        newArrow.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);


        //Instantiate slots and results
        for (int i = 0; i < CraftedItem.recipe.results.Count; i++)
        {
            GameObject newSlot = Instantiate(craftingSlot);
            newSlot.transform.SetParent(RecipePanel.transform);
            newSlot.transform.localScale = new Vector3(1f, 1f, 1f);

            GameObject itemObj = Instantiate(craftedItem);
            itemObj.transform.SetParent(newSlot.transform);
            itemObj.transform.localPosition = Vector3.zero;
            itemObj.transform.localScale = new Vector3(1f, 1f, 1f);
            //itemObj.GetComponent<Image>().sprite = database.FetchItemById(CraftedItem.recipe.results[i].id).sprite;///////////////////////////////////////////////////////////////////////
            itemObj.GetComponent<Image>().sprite = Handler.GetComponent<Requests>().spritesDictionary[database.FetchItemById(CraftedItem.recipe.results[i].id).slug];
            itemObj.GetComponent<CraftedItemData>().craftedItem = new CraftedItem(database.FetchItemById(CraftedItem.recipe.results[i].id), CraftedItem.recipe.results[i].amount, CraftedItem.recipe);
            //itemObj.name = itemObj.GetComponent<CraftedItemData>().craftedItem.item.Title;


            if (itemObj.GetComponent<CraftedItemData>().craftedItem.amount == 1)
            {
                itemObj.transform.GetChild(0).GetComponent<Text>().text = "";
            }
            else
            {
                itemObj.transform.GetChild(0).GetComponent<Text>().text = itemObj.GetComponent<CraftedItemData>().craftedItem.amount.ToString();
            }

        }

        RecipePanel.SetActive(true);
    }
}
