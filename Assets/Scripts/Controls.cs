﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controls : MonoBehaviour {

	public Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode> ();

	public Text right, left, jump, inventory;

	private GameObject currentKey;

	private Color32 normal = new Color32(255, 255, 255, 255);
	private Color32 selected = new Color32(239, 116, 36, 255);

	// Use this for initialization
	void Start () {

		keys.Add ("Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right", "D")));
		keys.Add ("Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left", "A")));
		keys.Add ("Jump", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Jump", "Space")));
		keys.Add ("Inventory", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Inventory", "E")));

		right.text = keys ["Right"].ToString (); 
		left.text = keys ["Left"].ToString (); 
		jump.text = keys ["Jump"].ToString (); 
		inventory.text = keys ["Inventory"].ToString (); 
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (keys ["Right"])) {

			//Do a move action
			Debug.Log("Right");
		}

		if (Input.GetKeyDown (keys ["Left"])) {

			//Do a move action
			Debug.Log("Left");
		}

		if (Input.GetKeyDown (keys ["Jump"])) {

			//Do a move action
			Debug.Log("Jump");
		}

		if (Input.GetKeyDown (keys ["Inventory"])) {

			//Do a move action
			Debug.Log("Inventory");
		}
			
	}

	void OnGUI(){
		if (currentKey != null) {
			Event e = Event.current;
			if (e.isKey) {
				
				keys [currentKey.name] = e.keyCode;
				currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
				currentKey.GetComponent<Image>().color = normal;
				currentKey = null;
			}
		}
	}

	public void ChangeKey(GameObject clicked){

		if (currentKey != null) {

			currentKey.GetComponent<Image>().color = normal;
		}

		currentKey = clicked;
		currentKey.GetComponent<Image> ().color = selected;
		currentKey.transform.GetChild(0).GetComponent<Text>().text = "Press any key";
	}

	public void SaveKeys(){
		foreach (var key in keys) {
			PlayerPrefs.SetString (key.Key, key.Value.ToString ());
			//"Keys: Up --> W"
		}

		PlayerPrefs.Save();
	
	}

	public void LoadDefaultKeys(){
		keys ["Right"] = (KeyCode)System.Enum.Parse(typeof(KeyCode), "D");
		right.text = keys ["Right"].ToString(); 

		keys ["Left"] = (KeyCode)System.Enum.Parse(typeof(KeyCode), "A");
		left.text = keys ["Left"].ToString(); 

		keys ["Jump"] = (KeyCode)System.Enum.Parse(typeof(KeyCode), "Space");
		jump.text = keys ["Jump"].ToString(); 

		keys ["Inventory"] = (KeyCode)System.Enum.Parse(typeof(KeyCode), "E");
		inventory.text = keys ["Inventory"].ToString(); 
	}
		
}
