﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public Text inventoryText, selectionText;

    int[] counts = new int[8];

    int selectedTile;

    string[] names = new string[] { "Dirt", "Grass", "Stone", "Coal", "Iron", "Gold", "Diamond", "Glass" };

    public GameObject[] tiles = new GameObject[8];
  

	void Start () {
		
	}


    void Update()
    {
        inventoryText.text = "Dirt: " + counts[0] + "\nGrass: " + counts[1] + "\nStone: " + counts[2] + "\nCoal: " + counts[3] + "\nIron: " + counts[4] + "\nGold: " + counts[5] + "\nDiamond: " + counts[6] + "\nGlass: " + counts[7];

        if (selectedTile < 0)
        {
            selectedTile = counts.Length - 1;
        }
        if (selectedTile > counts.Length - 1)
        {
            selectedTile = 0;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            selectedTile++;
        }

		if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            selectedTile--;
        }

        selectionText.text = "Selected Tile: " + names[selectedTile];

		if (Input.GetAxis ("Fire2")>0)
        {
            if (counts[selectedTile] > 0)
            {

                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                Vector3 placePos = new Vector3(Mathf.Round(mousePos.x), Mathf.Round(mousePos.y), 0f);

				var distance = Vector3.Distance(transform.position, placePos);



                if (Physics2D.OverlapCircleAll(placePos, 0.25f).Length == 0)
                {
					Debug.Log ("Distance: " + distance);


					if (distance <= 5) { 

						Instantiate (tiles [selectedTile], placePos, Quaternion.identity);
						counts [selectedTile]--;
					}
                }
            }
        }

        if (selectedTile == 0)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                if (counts[0] >= 2)
                {
                    counts[7]++;
                    counts[0] -= 2;
                }
            }
        }
    }

    public void Add (int tileType, int count)
    {
        counts[tileType] += count;
        // counts[tileType] = counts[tileType] + count
    }
}
