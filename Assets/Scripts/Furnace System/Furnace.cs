﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Furnace : MonoBehaviour
{
    public GameObject Handler;
    public UI_Handler ui;
    public GameObject canvas;
    public GameObject furnacePanel;
    public GameObject slotPanel;
    public GameObject mainPanel;

    /*
    public List<Item> burnableItems = new List<Item>();
    public List<Item> meltableItems = new List<Item>();
    public List<Item> resultItems = new List<Item>();

    public List<int> burnableItems = new List<int>();
    public List<int> meltableItems = new List<int>();
    public List<int> resultItems = new List<int>();
    */

    public List<Item> items = new List<Item>();
    public List<int> itemAmounts = new List<int>();
    public List<GameObject> slots = new List<GameObject>();

    public int meltingValue;
    public int burningValue;

    public GameObject meltingProgress;
    public GameObject burningProgress;

    public bool isMelting = false;

    public bool recentlyPlaced;
    public GameObject item;
    public GameObject Inventory;
    public InventoryNew InventoryNew;
    public ItemDatabase itemDatabase;
    public List<FurnaceRecipe> furnaceRecipes;
    public bool currentFurnaceIsSelected = false;

    public int furnaceSlotsStartingSlot;

    CraftingSystem CraftingSystem;

    FurnaceRecipe latestRecipe;
    Item latestBurnable;
    public int energyToMelt;


    // Start is called before the first frame update
    void Start()
    {
        meltingValue = 0;
        burningValue = 0;
        energyToMelt = 0;
        Handler = GameObject.Find("Handler");
        ui = Handler.GetComponent<UI_Handler>();

        Inventory = GameObject.Find("Inventory");
        CraftingSystem = Inventory.GetComponent<CraftingSystem>();
        InventoryNew = Inventory.GetComponent<InventoryNew>();
        itemDatabase = InventoryNew.GetComponent<ItemDatabase>();
        furnaceRecipes = Handler.GetComponent<FurnaceRecipeDatabase>().database;

        canvas = GameObject.Find("Canvas");
        //mainPanel = GameObject.Find("Main Panel");
        mainPanel = canvas.transform.GetChild(1).gameObject;
        //furnacePanel = GameObject.Find("Furnace Panel");
        furnacePanel = mainPanel.transform.GetChild(1).gameObject;

        meltingProgress = furnacePanel.transform.GetChild(2).GetChild(2).gameObject;
        burningProgress = furnacePanel.transform.GetChild(2).GetChild(3).gameObject;

        //slotPanel = furnacePanel.transform.GetChild(2).GetChild(0).gameObject;
        slotPanel = furnacePanel.transform.GetChild(2).GetChild(0).gameObject;

        furnaceSlotsStartingSlot = InventoryNew.furnaceSlotsStartingSlot;

        recentlyPlaced = true;

        //Items and slots list initial fill
        for (int i = 0; i < slotPanel.transform.childCount; i++)
        {
            items.Add(new Item());
            slots.Add(slotPanel.transform.GetChild(i).gameObject);
            //slots[i].GetComponent<SpecificInputSlot>().id = i;
            itemAmounts.Add(0);
        }



        //Required so that furnace will not be opened instantly after placement
        StartCoroutine(OpenDelay());

    }


    public void showFurnacePanel()
    {

        //Debug.Log("Canvas: " + canvas.name);
        //Debug.Log("mainPanel: " + mainPanel.name);


        //Making this furnace selected furnace in slots

        if (recentlyPlaced == false)
        {
            for (int i = 0; i < slotPanel.transform.childCount; i++)
            {
                /*
                if (i == 0 && slotPanel.transform.GetChild(i).gameObject.GetComponent<SpecificInputSlot>().furnace != null)
                    slotPanel.transform.GetChild(i).gameObject.GetComponent<SpecificInputSlot>().furnace.currentFurnaceIsSelected = false;
                    */
                slotPanel.transform.GetChild(i).gameObject.GetComponent<SpecificInputSlot>().currentFurnace = this.gameObject;
                slotPanel.transform.GetChild(i).gameObject.GetComponent<SpecificInputSlot>().furnace = this.gameObject.GetComponent<Furnace>();
                

                //Empty Inventory item slots
                //InventoryNew.items[furnaceSlotsStartingSlot + i] = new Item();



                //Destroy previous items from furnace panel
                //if (InventoryNew.slots[furnaceSlotsStartingSlot + i].transform.childCount > 0)
                    //Destroy(InventoryNew.slots[furnaceSlotsStartingSlot + i].transform.GetChild(0).gameObject);

                //Place this furnace items into furnace panel
                if (items[i].id != -1)
                {
                    InventoryNew.InsertItemsWithoutCheck(items[i].id, itemAmounts[i], (furnaceSlotsStartingSlot + i));
                }
            }

            //meltingProgress.transform.GetComponent<Slider>().maxValue = 100;
            meltingProgress.transform.GetComponent<Slider>().value = meltingValue;


            //burningProgress.transform.GetComponent<Slider>().maxValue = 1000;
            Debug.Log("Burning value: " + burningValue);
            burningProgress.transform.GetComponent<Slider>().value = burningValue;


            for (int i = 0; i < mainPanel.transform.childCount; i++)
            {
                if (mainPanel.transform.GetChild(i).name != "Inventory Panel")
                {
                    mainPanel.transform.GetChild(i).gameObject.SetActive(false);
                }

            }


            currentFurnaceIsSelected = true;
            furnacePanel.SetActive(true);
            ui.Open();


            if (recentlyPlaced == false)
            {

                furnacePanel.SetActive(true);
                ui.Open();
            }

        }

    }

    public bool canMelt()
    {
        //If not melting
        if (!isMelting)
        {
            //Debug.Log("1");
            furnaceRecipes = Handler.GetComponent<FurnaceRecipeDatabase>().database;
            //for (int i = 0; i < slotPanel.transform.childCount; i++)
            //{
                //If slot contains item
            if (items[0].id != 0)
            {
                //Debug.Log("2");
                //Recipes
                for (int j = 0; j < furnaceRecipes.Count; j++)
                {
                    bool melt = true;
                    latestRecipe = furnaceRecipes[j];
                    //Materials
                    for (int k = 0; k < furnaceRecipes[j].materials.Count; k++)
                    {                        
                            //If contains required id
                        if (items[0].id == furnaceRecipes[j].materials[k].id)
                        {
                            //Debug.Log("3");
                            //If contains required amount
                            if (itemAmounts[0] < furnaceRecipes[j].materials[k].amount)
                            {
                                //Debug.Log("-4");
                                //Debug.Log("WORKED");
                                melt = false;
                                break;
                            }
                            else
                            {
                                //Debug.Log("5");
                                //If no energy in furnace and no burnable
                                if (burningValue == 0)
                                {
                                    if (items[1].id != -1)
                                    {
                                        if (items[1].stats.burnEnergy == 0)
                                        {
                                            //Debug.Log("-6");
                                            melt = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        //Debug.Log("-7");
                                        melt = false;
                                        break;
                                    }
                                    
                                }
                            }
                        }
                        else
                        {
                            //Debug.Log("-8");
                            //Debug.Log("WORKED");
                            melt = false;
                            break;
                        }

                        if (melt)
                        {
                            //Debug.Log("CanMelt");
                            //StartCoroutine(Melt());
                        }
                        else
                        {
                            //Debug.Log("Can not Melt");
                        }

                        return melt;

                    }



                }
            }
        }

        //Debug.Log("Can not Melt");
        return false;



    }

    IEnumerator OpenDelay()
    {
        yield return new WaitForSeconds(0.2f);
        recentlyPlaced = false;
    }

    IEnumerator Melt(FurnaceRecipe furnaceRecipe)
    {
        energyToMelt = furnaceRecipe.energy;
        isMelting = true;
        if (itemAmounts[0] == furnaceRecipe.materials[0].amount)
        {
            items[0] = new Item();

            if(currentFurnaceIsSelected)
            InventoryNew.DeleteAllItemsFill(furnaceSlotsStartingSlot);
        }
        else
        {
            itemAmounts[0] -= furnaceRecipe.materials[0].amount;
            //Decrease items amounts

            if (currentFurnaceIsSelected)
            {
                InventoryNew.slots[furnaceSlotsStartingSlot].transform.GetChild(0).GetComponent<ItemData>().amount -= furnaceRecipe.materials[0].amount;
                if (InventoryNew.slots[furnaceSlotsStartingSlot].transform.GetChild(0).GetComponent<ItemData>().amount == 1)
                    InventoryNew.slots[furnaceSlotsStartingSlot].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "";
                else
                    InventoryNew.slots[furnaceSlotsStartingSlot].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = InventoryNew.slots[furnaceSlotsStartingSlot].transform.GetChild(0).GetComponent<ItemData>().amount.ToString();
            }
                
        }

        while(energyToMelt > 0)
        {
            if (burningValue > 0)
            {
                yield return new WaitForSeconds(1f);
                //progressBarValues = Vector3.Lerp(progressBarValues, new Vector3(--energyToMelt, --burningValue), 1f);
                
                
                energyToMelt--;
                burningValue--;

            }
            else
            {
                if(items[1].id != -1 && items[1].stats.burnEnergy > 0)
                {
                    latestBurnable = items[1];
                    if (itemAmounts[1] == 1)
                    {
                        burningValue += items[1].stats.burnEnergy;
                        items[1] = new Item();
                        itemAmounts[1] = 0;

                        if (currentFurnaceIsSelected)
                        {
                            InventoryNew.DeleteAllItemsFill(furnaceSlotsStartingSlot + 1);
                        }
                    }
                    else
                    {
                        burningValue += items[1].stats.burnEnergy;
                        itemAmounts[1]--;

                        if (currentFurnaceIsSelected)
                        {
                            InventoryNew.slots[furnaceSlotsStartingSlot + 1].transform.GetChild(0).GetComponent<ItemData>().amount--;
                            if (InventoryNew.slots[furnaceSlotsStartingSlot + 1].transform.GetChild(0).GetComponent<ItemData>().amount == 1)
                                InventoryNew.slots[furnaceSlotsStartingSlot + 1].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "";
                            else
                                InventoryNew.slots[furnaceSlotsStartingSlot + 1].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = InventoryNew.slots[furnaceSlotsStartingSlot + 1].transform.GetChild(0).GetComponent<ItemData>().amount.ToString();
                        }
                    }
                }
                else
                {
                    yield return new WaitForSeconds(0.1f);
                }
            }
            Debug.Log("Energy to melt: "+ energyToMelt);
            //yield return new WaitForSeconds(1f);
        }
        if (items[2].id == furnaceRecipe.results[0].id)
        {
            itemAmounts[2]++;
            if (currentFurnaceIsSelected)
            {
                InventoryNew.slots[furnaceSlotsStartingSlot + 2].transform.GetChild(0).GetComponent<ItemData>().amount++;
                InventoryNew.slots[furnaceSlotsStartingSlot + 2].transform.GetChild(0).GetChild(0).GetComponent<Text>().text = InventoryNew.slots[furnaceSlotsStartingSlot + 2].transform.GetChild(0).GetComponent<ItemData>().amount.ToString();
            }
        }
        else if (items[2].id == -1)
        {
            items[2] = new Item(InventoryNew.database.FetchItemById(furnaceRecipe.results[0].id));
            itemAmounts[2] = furnaceRecipe.results[0].amount;

            if (currentFurnaceIsSelected)
            {
                InventoryNew.InsertItemsWithoutCheck(items[2].id, itemAmounts[2], (furnaceSlotsStartingSlot + 2));
            }
        }

        isMelting = false;



        //yield return null;
    }

    public void updateFurnaceStats()
    {
        for (int i = 0; i < slotPanel.transform.childCount; i++)
        {
            // Debug.Log("Slots [i] child count: " + slots[i].transform.childCount);
            if (slots[i].transform.childCount > 0) {
                //Debug.Log("UPDATE IF STARTED");
                items[i] = new Item(slots[i].transform.GetChild(0).GetComponent<ItemData>().item);
                itemAmounts[i] = slots[i].transform.GetChild(0).GetComponent<ItemData>().amount;
            }
            else
            {
                //Debug.Log("UPDATE ELSE STARTED");
                items[i] = new Item();
                itemAmounts[i] = 0;
            }
            //slots.Add(slotPanel.transform.GetChild(i).gameObject);
            //slots[i].GetComponent<SpecificInputSlot>().id = i;

        }
    }

    public void updateFurnaceProgress()
    {
        if (latestRecipe != null)
        {
            if (energyToMelt == 0)
            {
                meltingProgress.transform.GetComponent<Slider>().value = 0;
                //meltingProgress.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = new Color32(0, 0, 0, 0);
            }
            else
            {
                //Update melting progressBar
                //meltingProgress.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = new Color32(93, 190, 51, 255);
                meltingProgress.transform.GetComponent<Slider>().value = (100 - ((float)energyToMelt / latestRecipe.energy * 100));
            }
        }

        Debug.Log("Melting progress value: " + meltingProgress.transform.GetComponent<Slider>().value);



        if (latestBurnable != null)
        {
            if (burningValue == 0)
            {
                burningProgress.transform.GetComponent<Slider>().value = 0;
                //burningProgress.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = new Color32(0, 0, 0, 0);
            }
            else
            {
                //Update burning progressBar
                //burningProgress.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = new Color32(217, 58, 58, 255);
                burningProgress.transform.GetComponent<Slider>().value = ((float)burningValue / latestBurnable.stats.burnEnergy * 100);
            }

        }
        else
        {
            burningProgress.transform.GetComponent<Slider>().value = 0;
            //burningProgress.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = new Color32(0, 0, 0, 0);
        }

        Debug.Log("Burning progress value: " + burningProgress.transform.GetComponent<Slider>().value);
    }

    public void cleanup()
    {
        for (int i = 0; i < slotPanel.transform.childCount; i++)
        {
            slotPanel.transform.GetChild(i).gameObject.GetComponent<SpecificInputSlot>().currentFurnace = null;
            slotPanel.transform.GetChild(i).gameObject.GetComponent<SpecificInputSlot>().furnace = null;
            currentFurnaceIsSelected = false;
            
            if(InventoryNew.slots[furnaceSlotsStartingSlot + i].transform.childCount > 0)
                InventoryNew.DeleteAllItemsFill(furnaceSlotsStartingSlot + i);               

        }
        //Reset crafting
        CraftingSystem.InitialDictionaryFill();
        CraftingSystem.CheckRecipes();
    }

    void Update()
    {
        if (!isMelting)
        {
            if (canMelt())
            {
                StartCoroutine(Melt(latestRecipe));
            }
        }

        if (currentFurnaceIsSelected)
        {            
            updateFurnaceStats();
            updateFurnaceProgress();
        }

        /*
        for (int i = 0; i < slots.Count; i++)
        {
            if(slots[i].transform.childCount < 1)
            {
                items[i] = new Item();
            }
        }
        */
        //updateFurnaceSlots();

        /*
        if (canMelt())
            melt();
            */
    }
    /*
    bool canMelt()
    {

    }

    IEnumerator melt()
    {

    }
    */
}
