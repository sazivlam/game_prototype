﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SpecificInputSlot : MonoBehaviour, IDropHandler
{
    public int id;
    private InventoryNew inv;
    public GameObject currentFurnace;
    public Furnace furnace;

    private void Start()
    {
        inv = GameObject.Find("Inventory").GetComponent<InventoryNew>();
    }

    void ItemExchange(ItemData droppedItem)
    {
        ItemData item = this.transform.GetChild(0).GetComponent<ItemData>();

        item.slot = droppedItem.slot;
        item.transform.SetParent(inv.slots[droppedItem.slot].transform);
        item.transform.position = inv.slots[droppedItem.slot].transform.position;


        droppedItem.slot = id;
        droppedItem.transform.SetParent(this.transform);
        droppedItem.transform.position = this.transform.position;

        inv.items[droppedItem.slot] = droppedItem.item;
        inv.items[item.slot] = item.item;

        furnace.updateFurnaceStats();
    }


    public void OnDrop(PointerEventData eventData)
    {


        Debug.Log("OnDrop");
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();

        //Tells Item to not drop away, because it landed on slot
        droppedItem.DropAway = false;

        Debug.Log(inv.items[id].id);

        /*
		if (droppedItem.id == this.transform.GetChild (0).GetComponent<ItemData> ().id) {
			
			this.transform.GetChild (0).GetComponent<ItemData> ().amount += droppedItem.amount;
			//this.transform.GetChild (0).transform.GetChild (0).transform.GetComponent<Text> ().GetComponent<Text> ().text = "";


			inv.items [droppedItem.slot].ID= -1;

			Destroy (droppedItem);


		
		
		}
		*/



        if (inv.items[id].id == -1)
        {
            Debug.Log("Slot DROP HAPPENED WHEN ID = -1");
            inv.items[droppedItem.slot] = new Item();
            inv.items[id] = droppedItem.item;
            droppedItem.slot = id;


        }
        else if (this.transform.childCount < 1)
        {

        }


        //Item exchange
        else if (droppedItem.id != this.transform.GetChild(0).GetComponent<ItemData>().id)
        {
            Debug.Log("Different Exchange");

            ItemExchange(droppedItem);



        }

        //Two identical items sum
        else if (droppedItem.id == this.transform.GetChild(0).GetComponent<ItemData>().id)
        {
            Debug.Log("Identical Exchange");
            int remainder = 0;

            ItemData item = this.transform.GetChild(0).GetComponent<ItemData>();

            //item.slot = droppedItem.slot;

            if (item.amount == inv.stackSize)
            {
                ItemExchange(droppedItem);
                return;
            }

            item.amount = item.amount + droppedItem.amount;


            if (item.amount > inv.stackSize)
            {
                remainder = (item.amount - inv.stackSize);
                item.amount = inv.stackSize;
                Debug.Log("Item.amount = " + item.amount + " - stackSize = " + inv.stackSize + " = " + (item.amount - inv.stackSize));

                droppedItem.amount = remainder;
            }

            inv.items[droppedItem.slot] = droppedItem.item;
            //inv.items[item.slot] = item.item;

            if (remainder == 0)
            {
                Debug.Log("REMAINDER = " + remainder);
                this.transform.GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text = this.transform.GetChild(0).GetComponent<ItemData>().amount.ToString();

                droppedItem.transform.SetParent(inv.slots[droppedItem.slot].transform);
                droppedItem.transform.position = inv.slots[droppedItem.slot].transform.position;
                inv.DeleteAllItemsFill(droppedItem.slot);
            }
            else
            {
                Debug.Log("REMAINDER = " + remainder);
                droppedItem.transform.SetParent(inv.slots[droppedItem.slot].transform);
                droppedItem.transform.position = inv.slots[droppedItem.slot].transform.position;

                if (remainder != 1)
                    inv.slots[droppedItem.slot].transform.GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text = remainder.ToString();
                else
                    inv.slots[droppedItem.slot].transform.GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text = "";

                inv.slots[item.slot].transform.GetChild(0).GetChild(0).GetComponent<UnityEngine.UI.Text>().text = inv.slots[item.slot].transform.GetChild(0).GetComponent<ItemData>().amount.ToString();
            }
        }
        furnace.updateFurnaceStats();
    }

}
