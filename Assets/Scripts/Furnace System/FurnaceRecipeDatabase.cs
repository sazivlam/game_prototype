﻿using System.Collections;
using UnityEngine;
using LitJson;
using System.Collections.Generic;
using System.IO;

public class FurnaceRecipeDatabase : MonoBehaviour
{
    public List<FurnaceRecipe> database;

    private void Start()
    {
        string json = File.ReadAllText(Application.dataPath + "/StreamingAssets/FurnaceRecipes.json");


        database = JsonMapper.ToObject<List<FurnaceRecipe>>(json);

        for (int i = 0; i < database.Count; i++)
        {
            //Debug.Log("Furnace Recipe: " + database[i].description);
        }


    }

    public FurnaceRecipe FetchFurnaceRecipeByID(int id)
    {
        for (int i = 0; i < database.Count; i++)
            if (database[i].id == id)
                return database[i];
        return null;

    }
}





public class FurnaceRecipe
{
    public int id { get; set; }
    public List<Ingredient> materials { get; set; }
    public int energy { get; set; }
    public List<Ingredient> results { get; set; }
    public string description { get; set; }
}
