﻿using System.Collections;
using UnityEngine;

public class GenerateChunk : MonoBehaviour
{

    public GameObject DirtTile;
    public GameObject GrassTile;
    public GameObject StoneTile;
    public GameObject AirTile;

    public int width;
    public int height;
    public float heightMultiplier;
    public int heightAddition;
    public float smoothness;

    public int DirtLayer;

    int skip_tiles;

    [HideInInspector]
    public float seed;

    public GameObject tileCoal;
    public GameObject tileIron;
    public GameObject tileGold;
    public GameObject tileDiamond;

    public int coalOreMinSize;
    public int ironOreMinSize;
    public int goldOreMinSize;
    public int diamondOreMinSize;

    public int coalOreMaxSize;
    public int ironOreMaxSize;
    public int goldOreMaxSize;
    public int diamondOreMaxSize;

    public float chanceCoal;
    public float chanceIron;
    public float chanceGold;
    public float chanceDiamond;

    public float chanceTree;

    public GameObject chunks;
    public GameObject Tree;
    public GameObject[,] World_GameObjects;


    // Use this for initialization
    void Start()
    {
        Generate();
    }



    public void Generate()
    {
        World_GameObjects = new GameObject[width, height];

        for (int i = 0; i < width; i++)
        {
            int h = Mathf.RoundToInt(Mathf.PerlinNoise(seed, (i + transform.position.x) / smoothness) * heightMultiplier) + heightAddition;

            Debug.Log("Int h: " + h);

            for (int j = 0; j < height; j++)
            {
                GameObject selectedTile;
                if (j < h - DirtLayer - 1)
                {
                    selectedTile = StoneTile;
                }
                else if (j < h - 1)
                {
                    selectedTile = DirtTile;
                }
                else if (j >= h)
                {
                    selectedTile = AirTile;
                }
                else
                {
                    selectedTile = GrassTile;
                }
                World_GameObjects[i, j] = (GameObject)Instantiate(selectedTile, Vector3.zero, Quaternion.identity);
                World_GameObjects[i, j].transform.parent = this.gameObject.transform;
                World_GameObjects[i, j].transform.localPosition = new Vector3(i, j);
            }
        }
        Populate(World_GameObjects);
    }

    public void Populate(GameObject[,] World_GameObjects)
    {
        skip_tiles = 0;
        int OreSize = 0;
        GameObject selectedTile = null;


        foreach (GameObject t in GameObject.FindGameObjectsWithTag("TileStone"))
        {
            Vector3 NewPosition = t.transform.localPosition;


            if (t.transform.parent == this.gameObject.transform)
            {
                float r = Random.Range(0f, 100f);


                if (r < chanceDiamond)
                {
                    selectedTile = tileDiamond;
                    OreSize = (int)Random.Range(diamondOreMinSize, diamondOreMaxSize);
                }
                else if (r < chanceGold)
                {
                    selectedTile = tileGold;
                    OreSize = (int)Random.Range(goldOreMinSize, goldOreMaxSize);
                }
                else if (r < chanceIron)
                {
                    selectedTile = tileIron;
                    OreSize = (int)Random.Range(ironOreMinSize, ironOreMaxSize);
                }
                else if (r < chanceCoal)
                {
                    selectedTile = tileCoal;
                    OreSize = (int)Random.Range(coalOreMinSize, coalOreMaxSize);
                }




                if (selectedTile != null)
                {
                    //Debug.Log("Position: " + (int)NewPosition.x + " , " + (int)NewPosition.y);

                    if (OreSize <= 0)
                    {
                        selectedTile = null;

                    }
                    else if (OreSize > 0)
                    {

                        // Setka 5x5 v vide 2 ciklov
                        for (int y = -2; y <= 2; y++)
                        {
                            for (int x = -2; x <= 2; x++)
                            {

                                // Smena pozicii na levij niznij ugol setki 5x5
                                NewPosition = new Vector3(t.transform.localPosition.x + x, t.transform.localPosition.y + y, 0f);

                                if (NewPosition.x >= 0 && NewPosition.y >= 0 && NewPosition.x <= 63 && NewPosition.y <= 127)
                                {
                                    if (OreSize > 0)
                                    {
                                        Destroy(World_GameObjects[(int)NewPosition.x, (int)NewPosition.y]);

                                        World_GameObjects[(int)NewPosition.x, (int)NewPosition.y] = (GameObject)Instantiate(selectedTile, Vector3.zero, Quaternion.identity);
                                        World_GameObjects[(int)NewPosition.x, (int)NewPosition.y].transform.parent = this.gameObject.transform;
                                        World_GameObjects[(int)NewPosition.x, (int)NewPosition.y].transform.localPosition = new Vector3((int)NewPosition.x, (int)NewPosition.y);

                                    }
                                }
                                OreSize--;
                            }
                        }
                        OreSize = 0;

                    }
                }
            }
        }

        foreach (GameObject t in GameObject.FindGameObjectsWithTag("TileGrass"))
        {
            

            if (skip_tiles == 0)
            {


                selectedTile = null;
                Vector3 NewPosition = t.transform.localPosition;


                if (t.transform.parent == this.gameObject.transform)
                {
                    float r = Random.Range(0f, 100f);


                    if (r < chanceTree)
                    {
                        selectedTile = Tree;
                        //skip_tiles = Random.Range(1, 5);

                        //leaves width = 2 => skip_tiles = 2
                        skip_tiles = 2;
                    }


                    if (selectedTile != null)
                    {
                        //Debug.Log(" YEAH TREE IS PLACED AT Position: " + (int)NewPosition.x + " , " + (int)NewPosition.y);
                        NewPosition = new Vector3(t.transform.localPosition.x, t.transform.localPosition.y + 1.0f, 0f);

                        if (NewPosition.x >= 2 && NewPosition.x < width - 2 && NewPosition.y < height)
                        {
                            Destroy(World_GameObjects[(int)NewPosition.x, (int)NewPosition.y]);

                            //World_GameObjects[(int)NewPosition.x, (int)NewPosition.y] = (GameObject)Instantiate(selectedTile, Vector3.zero, Quaternion.identity);
                            GameObject NewTree = (GameObject)Instantiate(selectedTile, Vector3.zero, Quaternion.identity);

                            //GameObject NewTree = World_GameObjects[(int)NewPosition.x, (int)NewPosition.y];

                            //World_GameObjects[(int)NewPosition.x, (int)NewPosition.y].transform.parent = this.gameObject.transform;
                            //World_GameObjects[(int)NewPosition.x, (int)NewPosition.y].transform.localPosition = new Vector3((int)NewPosition.x, (int)NewPosition.y);

                            NewTree.transform.parent = this.gameObject.transform;
                            NewTree.transform.localPosition = new Vector3((int)NewPosition.x, (int)NewPosition.y);
                        }
                        else
                            skip_tiles = 0;

                    }
                }
            }
            else
            {
                skip_tiles--;
            }
        }

    }
}