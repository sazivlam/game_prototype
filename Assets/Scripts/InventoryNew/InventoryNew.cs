﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryNew : MonoBehaviour {

    GameObject inventoryPanel;
    GameObject slotPanel;
	GameObject quickAccessPanel;

	GameObject armorPanel;
	GameObject acessoriesPanel;

    GameObject furnacePanel;
    GameObject furnaceSlotPanel;

    public ItemDatabase database;
    public UI_Handler ui;
    public GameObject inventorySlot;

    public GameObject inventoryItem;
    public GameObject Handler;

	public int selectedSlot=0;
	public ItemData selectedItem;

    public int quickAccessPanelAmount = 10;
    public int slotPanelAmount = 30;
	//int armorPanelAmount = 4;
	int acessoriesPanelAmount = 8;
    int furnaceSlotPanelAmount = 3;
    int currentSlot = 0;
    public int furnaceSlotsStartingSlot;

    public int stackSize = 99;

    List<GameObject> panelsToFill;
    List<int> panelSlotsAmounts;
    public List<Item> items = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();

    CraftingSystem CraftingSystem;



    private void Start()
    {
        Handler = GameObject.Find("Handler");
        CraftingSystem = this.gameObject.GetComponent<CraftingSystem>();

        database = GetComponent<ItemDatabase>();
        ui = Handler.GetComponent<UI_Handler>();

        inventoryPanel = GameObject.Find("Inventory Panel");
        quickAccessPanel = GameObject.Find("Quick Access Panel");
        slotPanel = inventoryPanel.transform.Find("Slot Panel").gameObject;
		//armorPanel = inventoryPanel.transform.Find("Armor Panel").gameObject;
		acessoriesPanel = GameObject.Find("Acessories Panel").gameObject;
        furnacePanel = GameObject.Find("Furnace Panel");
        furnaceSlotPanel = GameObject.Find("Furnace Slot Panel");

        panelsToFill = new List<GameObject> { quickAccessPanel, slotPanel, /*armorPanel,*/  acessoriesPanel };
        panelSlotsAmounts = new List<int>   { quickAccessPanelAmount, slotPanelAmount, /*armorPanelAmount,*/ acessoriesPanelAmount };


        //Filling Panels with Slots
        for (int i = 0; i < panelsToFill.Count;i++)
        {
            for (int j = 0; j < panelSlotsAmounts[i]; j++)
            {
                items.Add(new Item());
                slots.Add(Instantiate(inventorySlot));
                slots[currentSlot].GetComponent<Slot>().id = currentSlot;
                slots[currentSlot].transform.SetParent(panelsToFill[i].transform);
                slots[currentSlot].transform.localScale = new Vector3(1f, 1f, 1f);
                currentSlot++;
            }
        }

        furnaceSlotsStartingSlot = currentSlot;

        //Adding furnace slots to items and slots lists
        for (int i = 0; i < furnaceSlotPanelAmount; i++)
        {
            items.Add(new Item());
            slots.Add(furnaceSlotPanel.transform.GetChild(i).gameObject);
            slots[currentSlot].GetComponent<SpecificInputSlot>().id = currentSlot;
            //slots[currentSlot].transform.SetParent(panelsToFill[i].transform);
            //slots[currentSlot].transform.localScale = new Vector3(1f, 1f, 1f);
            currentSlot++;
        }


        //selectedSlot = 0;
        if (slots [selectedSlot].transform.childCount > 0) {

			selectedItem = slots [selectedSlot].transform.GetChild (0).GetComponent<ItemData>();

        } else {
			selectedItem = null;
		}

        //Make selected slot yellow 
        slots[selectedSlot].GetComponent<Image>().color = new Color32(255, 255, 0, 255);

        furnacePanel.SetActive(false);
        Handler.GetComponent<UI_Handler>().Close();

    }

	void Update(){
        //Selected Slot Update
        if (!ui.IsAnything_Showing()) {
            if (Input.GetAxis("Mouse ScrollWheel") < 0 && selectedSlot < (quickAccessPanelAmount - 1))
            {
                UpdateQuickAccessPanel(selectedSlot, ++selectedSlot);

            } else if (Input.GetAxis("Mouse ScrollWheel") > 0 && selectedSlot > 0) {

                UpdateQuickAccessPanel(selectedSlot, --selectedSlot);

            }
        }

		CheckForSelectedTile (selectedSlot);
    }

    public void UpdateQuickAccessPanel(int previousSelectedSlot, int selectedSlot)
    {
        //Make previous selected slot white 
        slots[previousSelectedSlot].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        //Make new selected slot yellow
        slots[selectedSlot].GetComponent<Image>().color = new Color32(255, 255, 0, 255);

    }

	//Selected Slot Update
	public void CheckForSelectedTile(int selectedSlot){
		//Debug.Log ("Selected Slot: " + selectedSlot);
		if (slots [selectedSlot].transform.childCount > 0) {
			//Debug.Log ("Selected tile: " + slots [selectedSlot].transform.GetChild (0).GetComponent<ItemData>().name);
			selectedItem = slots [selectedSlot].transform.GetChild (0).GetComponent<ItemData>();

        } else {
			selectedItem = null;
		}

	}



    public int AddItems(int id, int amount)
    {
        int addedItemAmount = 0;
        for (int i = 0; i < amount; i++)
        {
            if (AddItem(id))
            {
                addedItemAmount++;
            }
            else
            {
                break;
            }
        }
        CraftingSystem.UpdateDictionary(id, addedItemAmount);
        return addedItemAmount;

    }






    //Item Add
    public bool AddItem(int id)
    {
        //Debug.Log("Add Item start");
        //Item itemToAdd = database.FetchItemById(id);
        int slotToAdd = canAdd(id);

        //if item can be added
        if (slotToAdd != -1)
        {
            //if item is already in the inventory, then add to it this one
            if (items[slotToAdd].id == id)
            {
                AddItem(id, slotToAdd);
                return true;
            }
            //if item is not in the inventory, then add it separetly
            else
            {
                AddItemSeparetly(id, slotToAdd);
                return true;
            }
        }
        //if item can not be added, then return false
        else
            return false;
    }

        //return false;

    

    public bool AddItem(int id, int slot)
    {
        //Item itemToAdd = database.FetchItemById(id);
        ItemData data = slots[slot].transform.GetChild(0).GetComponent<ItemData>();

        slots[slot].transform.GetChild(0).localPosition = Vector3.zero;
        slots[slot].transform.GetChild(0).localScale = new Vector3(1f, 1f, 1f);
        slots[slot].transform.GetChild(0).GetComponent<ItemData>().amount += 1;

        data.transform.GetChild(0).GetComponent<Text>().text = slots[slot].transform.GetChild(0).GetComponent<ItemData>().amount.ToString();
        return true;
    }



    bool AddItemSeparetly(int id, int slot)
    {
        Item itemToAdd = database.FetchItemById(id);

        //Debug.Log("Add Item Separetly GOOD");
        items[slot] = itemToAdd;
        GameObject itemObj = Instantiate(inventoryItem);
        itemObj.GetComponent<ItemData>().item = itemToAdd;
        itemObj.GetComponent<ItemData>().amount = 1;
        itemObj.GetComponent<ItemData>().slot = slot;


        itemObj.GetComponent<ItemData>().id = id;

        itemObj.transform.SetParent(slots[slot].transform);
        itemObj.transform.localPosition = Vector3.zero;
        itemObj.transform.localScale = new Vector3(1f, 1f, 1f);
        itemObj.GetComponent<Image>().sprite = Handler.GetComponent<Requests>().spritesDictionary[itemToAdd.slug];
        itemObj.name = itemToAdd.title;
        return true;
    }



    public bool AddItemSeparetly(int id)
    {
        Item itemToAdd = database.FetchItemById(id);
        int slot = canAddSeparetly(id);

        if (slot != -1)
        {
            Debug.Log("Add Item Separetly GOOD");
            items[slot] = itemToAdd;
            GameObject itemObj = Instantiate(inventoryItem);
            itemObj.GetComponent<ItemData>().item = itemToAdd;
            itemObj.GetComponent<ItemData>().amount = 1;
            itemObj.GetComponent<ItemData>().slot = slot;

            itemObj.GetComponent<ItemData>().id = id;

            itemObj.transform.SetParent(slots[slot].transform);
            itemObj.transform.localPosition = Vector3.zero;
            itemObj.transform.localScale = new Vector3(1f, 1f, 1f);
            itemObj.GetComponent<Image>().sprite = Handler.GetComponent<Requests>().spritesDictionary[itemToAdd.slug];
            itemObj.name = itemToAdd.title;
            //ItemData data = slots[i].transform.GetChild(0).GetComponent<ItemData>();
            return true;
        }
        return false;
    }

    public bool InsertItemsWithoutCheck(int id, int amount, int slot)
    {
        Item itemToAdd = database.FetchItemById(id);

        if (slot != -1)
        {
            items[slot] = itemToAdd;
            GameObject itemObj = Instantiate(inventoryItem);
            itemObj.GetComponent<ItemData>().item = itemToAdd;
            itemObj.GetComponent<ItemData>().amount = amount;
            itemObj.GetComponent<ItemData>().slot = slot;

            itemObj.GetComponent<ItemData>().id = id;

            itemObj.transform.SetParent(slots[slot].transform);
            itemObj.transform.localPosition = Vector3.zero;
            itemObj.transform.localScale = new Vector3(1f, 1f, 1f);
            itemObj.GetComponent<Image>().sprite = Handler.GetComponent<Requests>().spritesDictionary[itemToAdd.slug];
            itemObj.name = itemToAdd.title;

            if (itemObj.GetComponent<ItemData>().amount == 1)
                itemObj.transform.GetChild(0).GetComponent<Text>().text = "";
            else
                itemObj.transform.GetChild(0).GetComponent<Text>().text = itemObj.GetComponent<ItemData>().amount.ToString();
            //ItemData data = slots[i].transform.GetChild(0).GetComponent<ItemData>();
            return true;
        }
        return false;

    }





    public void DeleteItems(int id, int amount)
    {
        for (int i = 0; i < amount; i++)
            DeleteItem(id);
    }


    //Item delete
    public void DeleteItem(int id) {

        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].id == id)
            {
                DeleteItem(id, i);
                return;
            }
        }

    }




    //Item delete (nado dorabotatj)
    public void DeleteItem(int id, int slot)
    {
        //Debug.Log("DeleteItemWorked");
        int i = slot;
        ItemData data = slots[i].transform.GetChild(0).GetComponent<ItemData>();
                    if (data.amount > 1)
                    {
                        slots[i].transform.GetChild(0).localPosition = Vector3.zero;
                        slots[i].transform.GetChild(0).localScale = new Vector3(1f, 1f, 1f);
                        data.amount--;

                        if (data.amount == 1)
                            data.transform.GetChild(0).GetComponent<Text>().text = "";
                        else
                            data.transform.GetChild(0).GetComponent<Text>().text = data.amount.ToString();

                    }
                    else
                    {
                    items[i] = new Item();
                    Destroy(slots[i].transform.GetChild(0).gameObject);

        }
        //Updates item dictionary, to check for crafts
        CraftingSystem.UpdateDictionary(id, -1);
    }



    public void DeleteAllItems(int slot)
    {
        CraftingSystem.UpdateDictionary(slots[slot].transform.GetChild(0).gameObject.GetComponent<ItemData>().id, -slots[slot].transform.GetChild(0).gameObject.GetComponent<ItemData>().amount);
        Destroy(slots[slot].transform.GetChild(0).gameObject);
        //Destroy(slots[i + 53].transform.GetChild(0).gameObject);
        
        items[slot] = new Item();

    }

    public void DeleteAllItemsFill(int slot)
    {
        Destroy(slots[slot].transform.GetChild(0).gameObject);
        items[slot] = new Item();
    }

    public int canAdd(int id)
    {

        Item itemToAdd = database.FetchItemById(id);
        if (itemToAdd.tags.stackable == true)
        {
            //for (int i = 0; i < items.Count; i++)
            for (int i = 0; i < (quickAccessPanelAmount + slotPanelAmount); i++)
            {
                //If item is in inventory
                if (items[i].id == id)
                {
                    if (slots[i].transform.GetChild(0).GetComponent<ItemData>().amount < stackSize)
                    {
                        //Debug.Log("Pered 1. true"); 
                        return i;
                    }
                }
            }
        }

        return canAddSeparetly(id);
    }

    public int canAddSeparetly(int id)
    {
        // -1 means that you can not add item
        // !-1 means you can add item and the slot position where to add is result
        int result = -1;

        //for (int i = 0; i < items.Count; i++)
        for (int i = 0; i < (quickAccessPanelAmount + slotPanelAmount); i++)
        {
            if (items[i].id == -1)
                return i;
        }

        return result;
    }

    bool CheckIfItemIsInInventory(Item item)
    {
        //for (int i = 0; i < items.Count; i++)
        for (int i = 0; i < (quickAccessPanelAmount + slotPanelAmount); i++)
            if (items[i].id == item.id)
                return true;
        return false;
    }
}
