﻿using System.Collections;
using UnityEngine;
using LitJson;
using System.Collections.Generic;
using System.IO;

public class ItemDatabase : MonoBehaviour {
    private List<Item> database = new List<Item>();
    //private JsonData itemData;

    private void Start()
    {
        //itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
        ConstructItemDatabase();

    }

    public Item FetchItemById(int id)
    {
        for (int i = 0; i < database.Count; i++)
            if(database[i].id == id)
            return database[i];
        return null;
        
    }

    public Item FetchItemByTitle(string title)
    {
        for (int i = 0; i < database.Count; i++)
            if (database[i].title == title)
                return database[i];
        return null;

    }


    void ConstructItemDatabase()
    {
        string json = File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json");

        database = JsonMapper.ToObject<List<Item>>(json);


        //Debug.Log("Items Count : " + database.Count);

        /*
        foreach(Item item in database)
        {
            Debug.Log("id : " + item.id);
            Debug.Log("title : " + item.title);
            Debug.Log("value : " + item.value);
            Debug.Log("stats power : " + item.stats.power);
            Debug.Log("stats defence : " + item.stats.defence);
            Debug.Log("stats vitality : " + item.stats.vitality);
            Debug.Log("stats strength : " + item.stats.strength);
            Debug.Log("tags touchable : " + item.tags.touchable);
            Debug.Log("tags placeable : " + item.tags.placeable);
            Debug.Log("tags stackable : " + item.tags.stackable);
            Debug.Log("description : " + item.description);
            Debug.Log("rarity : " + item.rarity);
            Debug.Log("slug: " + item.slug);
        }
        */




        /*
        for(int i=0; i< itemData.Count; i++)
        {
            database.Add(new Item((int)itemData[i]["id"], itemData[i]["title"].ToString(), (int)itemData[i]["value"],
                new Stats(itemData[i]["stats"]), itemData[i]["tags"], itemData[i]["description"].ToString(), (int)itemData[i]["rarity"], itemData[i]["slug"].ToString()));
        }
        */
        

    }
}


public class Item
{
    public int id { get; set; }
    public string title { get; set; }
    public int value { get; set; }
    public Stats stats {get; set; }
    public Tags tags { get; set; }
    //public int Power { get; set; }
    //public int Defence { get; set; }
    //public int Vitality { get; set; }
    public string description { get; set; }
    //public bool Stackable { get; set; }
    public int rarity { get; set; }
    public string slug { get; set; }
    //public Sprite sprite { get; set; }
    //public Dictionary<string, int> defaultStats;
    //public Dictionary<string, bool> defaultTags;
    //Placeable
    //Strength
    //Touchable


    public Item(int id, string title, int value, Stats stats, Tags tags, string description, int rarity, string slug)
    {
        //Debug.Log("Complex constructor");
        
        this.id = id;
        this.title = title;
        this.value = value;
        
        // stats = new Dictionary<string, int>();
       // tags = new Dictionary<string, bool>();

        /*
        //Fill Stats with default values
        defaultStats.Add("power", 0);
        defaultStats.Add("defence", 0);
        defaultStats.Add("vitality", 0);
        defaultStats.Add("strength", 10);

        //FillTags with default values
        defaultTags.Add("touchable", true);
        defaultTags.Add("placeable", true);
        defaultTags.Add("stackable", true);
        

        foreach (Stat s in stats)
            this.stats.Add(s.name, s.value);

        foreach (Tag t in tags)
            this.tags.Add(t.name, t.value);
            */

        this.description = description;
        this.rarity = rarity;
        this.slug = slug;
        
        /*
		if (id < 500) {

			this.sprite = Resources.Load<Sprite> ("Sprites/Basics/" + slug);

		} else if (id >= 500 && id < 750) {
			
			this.sprite = Resources.Load<Sprite> ("Sprites/Ores/" + slug);

		} else {
			this.sprite = Resources.Load<Sprite> ("Sprites/Items/" + slug);
		}
        */
        
    }

    public Item(Item item)
    {
        this.id = item.id;
        this.title = item.title;
        this.value = item.value;
        this.stats = item.stats;
        this.tags = item.tags;
        this.description = item.description;
        this.rarity = item.rarity;
        this.slug = item.slug;
    }


    public Item()
    {
        //Debug.Log("Default constructor");
        id = -1;
        /*
        stats = new Dictionary<string, int>();
        tags = new Dictionary<string, bool>();

        
        //Fill Stats with default values
        stats.Add("power", 0);
        stats.Add("defence", 0);
        stats.Add("vitality", 0);
        stats.Add("strength", 10);


        //FillTags with default values
        tags.Add("touchable", true);
        tags.Add("placeable", true);
        tags.Add("stackable", true);

        Debug.Log("Title: " + this.title);
        Debug.Log("stackable: " + tags["stackable"]);
        */
    }


    public class Stats
    {
        public int power { get; set; }
        public int defence { get; set; }
        public int vitality { get; set; }
        public int strength { get; set; }
        public int burnEnergy { get; set; }

        public Stats()
        {
            //Debug.Log("Stats default constructer");
            power = 0;
            defence = 0;
            vitality = 0;
            strength = 10;
            burnEnergy = 0;
        }
    }

    public class Tags
    {
        public bool complex { get; set; }
        public bool touchable { get; set; }
        public bool placeable { get; set; }
        public bool stackable { get; set; }

        public Tags()
        {
            //Debug.Log("Tags default constructer");
            complex = true;
            touchable = true;
            placeable = true;
            stackable = true;
        }
    }
}