﻿using System.Collections;
using UnityEngine;

public class DroppedItemData : MonoBehaviour
{
    public int id;
    public int amount;

    public CircleCollider2D innerCollider;

    //public GameObject Handler;

    //private InventoryNew inv;
    //private CircleCollider2D outerCollider;
    //private Rigidbody2D rb;

    public bool isMoving;

    private void Start()
    {
        
        //inv = GameObject.Find("Inventory").GetComponent<InventoryNew>();
        isMoving = false;
        
    }





    /*
    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log(col.gameObject.name + " : " + gameObject.name + " : " + Time.time);
        if(col.gameObject.name == "Character" && isMoving == false)
        {
            Debug.Log("Yeah");
            isMoving = true;
            StartCoroutine(MoveToPosition(this.transform, col.gameObject, 0.3f));
        }
        
        
    }


    public IEnumerator MoveToPosition(Transform object_to_move, GameObject character, float timeToMove)
    {
        Debug.Log("Yeah2");
        var currentPos = object_to_move.position;
        var t = 0f;
        innerCollider.enabled = false;

        while (t < 1)
        {
            t += Time.deltaTime / timeToMove; 
            object_to_move.position = Vector3.Lerp(currentPos, character.transform.position, t);
            yield return null;
        }
        inv.AddItem(this.id);
        Destroy(this.gameObject);
    }
    */
}
