﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Requests : MonoBehaviour
{
    public GameObject Tile;
    public GameObject Furnace;
    GameObject Chunks;
    GameObject Character;
    //int chunk_height;
    int chunk_width;
    int global_tile_hierarchy_number;
    //Material Base_material;
    //List<Sprite> sprites = new List<Sprite>();
    public Dictionary<string, Sprite> spritesDictionary;
    // Start is called before the first frame update

    public GameObject Inventory;

    void Start()
    {
        Chunks = GameObject.Find("Chunks(Clone)");
        Character = GameObject.Find("Character");
        //chunk_height = Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().height;
        chunk_width = Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().width;
        //Base_material = Resources.Load<Material>("Materials/" + "Base_Material");
        spritesDictionary = new Dictionary<string, Sprite>();
        LoadResources();
    }

    // Update is called once per frame
    /*
    void Update()
    {
        
    }
    */

    public void LoadResources()
    {
        string[] spriteFolders = new string[] { "Sprites/Basics", "Sprites/Items", "Sprites/Ores" };

        foreach (string spriteFolder in spriteFolders)
        {
            object[] loadedIcons = Resources.LoadAll(spriteFolder, typeof(Sprite));
            //Debug.Log("loadedIcons length: " + loadedIcons.Length);

            for (int x = 0; x < loadedIcons.Length; x++)
            {
                Sprite currentSprite = (Sprite)loadedIcons[x];
                //Debug.Log("currentSprite name: " + currentSprite.name);

                spritesDictionary.Add(currentSprite.name, currentSprite);
            }
        }
    }

    public GameObject PlaceItem(Transform place, int id)
    {
        Item addedItem = Inventory.GetComponent<InventoryNew>().database.FetchItemById(id);

        //Random value range
        float RandomRange = 0.25f;

        //Random spawn place
        Vector3 placePosition = new Vector3(place.position.x + Random.Range(-RandomRange, RandomRange), place.position.y + Random.Range(-RandomRange, RandomRange), -0.01f);

        GameObject NewObject = Instantiate(Resources.Load<GameObject>("Tiles/Items/" + "(Dropped Item)"), placePosition, Quaternion.identity) as GameObject;


        // Name correction
        NewObject.name = NewObject.name.Replace("(Clone)", "");
        NewObject.name = addedItem.title + " " + NewObject.name;


        /*
        //Material change
        Material tileMaterial = Base_material;
        tileMaterial.shader = Shader.Find("Standard");
        tileMaterial.EnableKeyword("_DetailAlbedoMap");
        tileMaterial.SetTexture("_DetailAlbedoMap", spritesDictionary[addedItem.Slug]);

    */
    /*
        Material[] mats = NewObject.GetComponent<MeshRenderer>().materials;
        //Debug.Log("NAME: " + addedItem.Title.Replace("Tile", ""));

        if(IsOre(id))
            mats[0] = Resources.Load<Material>("Materials/Ores/" + addedItem.title); 
        else
            mats[0] = Resources.Load<Material>("Materials/Basics/" + addedItem.title);

        NewObject.GetComponent<MeshRenderer>().materials = mats;
        */

        NewObject.GetComponent<SpriteRenderer>().sprite = spritesDictionary[addedItem.slug];


        //Id change
        NewObject.GetComponent<DroppedItemData>().id = id;

        return NewObject;
        
    }

    public GameObject PlaceItem(Transform place, int id, int amount)
    {
        GameObject NewItem = PlaceItem(place, id);
        NewItem.GetComponent<DroppedItemData>().amount = amount;

        return NewItem;

    }

    public void DropItems(int id, int amount)
    {
        StartCoroutine(DropItemFromInventoryCoroutine(id, amount));
    }

    public void DropItemFromInventory(Transform place, int id, int amount, int slot)
    {
        StartCoroutine(DropItemFromInventoryCoroutine(place, id, amount, slot));
    }



    IEnumerator DropItemFromInventoryCoroutine(Transform place, int id, int amount, int slot)
    {
        //Disable circle collider
        Character.GetComponent<CircleCollider2D>().enabled = false;
        
        //Place New Dropped Item
        GameObject NewItem = PlaceItem(place, id, amount);
        float force = 170f;

        
        if (Character.GetComponent<Character>().face_right == true)
        {
            //Move abit from Character
            NewItem.transform.position = NewItem.transform.position + new Vector3(1f, 0f);
            //Push Item
            NewItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(force, force));
        }
        else
        {
            NewItem.transform.position = NewItem.transform.position + new Vector3(-1f, 0f);
            NewItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(-force, force));
        }

        //Delete Item from inventory
        Inventory.GetComponent<InventoryNew>().DeleteAllItems(slot);

        yield return new WaitForSeconds(0.8f);

        //Enable circle collider
        Character.GetComponent<CircleCollider2D>().enabled = true;
       
    }

    IEnumerator DropItemFromInventoryCoroutine(int id, int amount)
    {
        //Disable circle collider
        Character.GetComponent<CircleCollider2D>().enabled = false;
        Transform charactersTransform = Character.GetComponent<Transform>();

        //Place New Dropped Item
        GameObject NewItem = PlaceItem(charactersTransform, id, amount);
        float force = 170f;


        if (Character.GetComponent<Character>().face_right == true)
        {
            //Move abit from Character
            NewItem.transform.position = NewItem.transform.position + new Vector3(1f, 0f);
            //Push Item
            NewItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(force, force));
        }
        else
        {
            NewItem.transform.position = NewItem.transform.position + new Vector3(-1f, 0f);
            NewItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(-force, force));
        }

        yield return new WaitForSeconds(0.8f);

        //Enable circle collider
        Character.GetComponent<CircleCollider2D>().enabled = true;

    }


    public GameObject PlaceTile (Vector3 place, int id)
    {
        Item item = Inventory.GetComponent<InventoryNew>().database.FetchItemById(id);
        string placedTileTitle = item.title;
        GameObject newTile;
        int chunk_number = Mathf.FloorToInt(place.x / Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().width);
        int local_position_x = (int)place.x - ((int)chunk_width * chunk_number);
        //int local_position_y = (int)place.y - ((int)chunk_height * chunk_number);

        //Previous Tile destroy
        DestroyTile(place);

        /*
        //Tile Placement
        if (id < 500)
        {
            newTile = Instantiate(Resources.Load<GameObject>("Tiles/Basic Tiles/" + placed_tile_title), place, Quaternion.identity) as GameObject;
        }
        else
        {
            newTile = Instantiate(Resources.Load<GameObject>("Tiles/Ores/" + placed_tile_title), place, Quaternion.identity) as GameObject;
        }
        */
        if (!item.tags.complex)
        {
            newTile = Instantiate(Tile, place, Quaternion.identity) as GameObject;


            if (placedTileTitle != "Air Tile")
            {
                newTile.GetComponent<SpriteRenderer>().sprite = spritesDictionary[item.slug];
                newTile.GetComponent<SpriteRenderer>().size = new Vector2(1, 1);
            }
        }
        else
        {
            newTile = Instantiate(Furnace, place, Quaternion.identity) as GameObject;
        }

        newTile.GetComponent<TileData>().id = id;
        newTile.GetComponent<TileData>().Strength = item.stats.strength;

        //NameCorrection
        newTile.name = placedTileTitle;

        // Adding new block to a chunk
        //Debug.Log("Chunk Number" + Chunks.transform.GetChild(chunk_number));
        newTile.transform.SetParent(Chunks.transform.GetChild(chunk_number));


        //Placing in right place in hierarchy
        //newTile.transform.SetSiblingIndex((Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().height * (int)newTile.transform.localPosition.x) + (int)newTile.transform.localPosition.y);

        newTile.transform.parent.transform.GetComponent<GenerateChunk>().World_GameObjects[local_position_x, (int)place.y] = newTile;



        if(placedTileTitle == "Air Tile")
        {
            Destroy(newTile.GetComponent<BoxCollider2D>());
            newTile.GetComponent<SpriteRenderer>().sprite = null;
        }
        if (item.tags.touchable == false)
        {
            newTile.GetComponent<BoxCollider2D>().isTrigger = true;
        }
        


        return newTile;
    }
    



    public GameObject PlaceTile(Vector3 place, string title)
    {
        Item item = Inventory.GetComponent<InventoryNew>().database.FetchItemByTitle(title);
        //Debug.Log("Item ID: " + item.id);
        return PlaceTile(place, item.id);
        /*
        GameObject newTile;
        int chunk_number = Mathf.FloorToInt(place.x / Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().width);
        int local_position_x = (int)place.x - ((int)chunk_width * chunk_number);
        //int local_position_y = (int)place.y - ((int)chunk_height * chunk_number);

        //Previous Tile destroy
        DestroyTile(place);

        //Tile Placement
        if (!title.Contains("Ore"))
        {
            newTile = Instantiate(Resources.Load<GameObject>("Tiles/Basic Tiles/" + title), place, Quaternion.identity) as GameObject;
        }
        else
        {
            newTile = Instantiate(Resources.Load<GameObject>("Tiles/Ores/" + title), place, Quaternion.identity) as GameObject;
        }

        // Adding new block to a chunk
        newTile.transform.SetParent(Chunks.transform.GetChild(chunk_number));


        //Placing in right place in hierarchy
        //newTile.transform.SetSiblingIndex((Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().height * (int)newTile.transform.localPosition.x) + (int)newTile.transform.localPosition.y);
        newTile.transform.parent.transform.GetComponent<GenerateChunk>().World_GameObjects[local_position_x, (int)place.y] = newTile;

        return newTile;
        */
    }

    public GameObject PlaceTile(Vector3 place, int id, int layer)
    {
        GameObject placedTile = PlaceTile(place, id);
        placedTile.layer = layer;
        return placedTile;
    }

    public GameObject PlaceTile(Vector3 place, string title, int layer)
    {
        GameObject placedTile = PlaceTile(place, title);
        placedTile.layer = layer;
        return placedTile;
    }



    public GameObject GetTile(Vector3 place)
    {
        int chunk_number = Mathf.FloorToInt(place.x / Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().width);
        int local_position_x = (int)place.x - ((int)chunk_width * chunk_number);
        return Chunks.transform.GetChild(chunk_number).transform.GetComponent<GenerateChunk>().World_GameObjects[local_position_x, (int)place.y].gameObject;

    }




    public void DestroyTile(Vector3 place)
    {
        int chunk_number = Mathf.FloorToInt(place.x / Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().width);
        int local_position_x = (int)place.x - ((int)chunk_width * chunk_number);
        //int local_position_y = (int)place.y - ((int)chunk_height * chunk_number);

        Destroy(Chunks.transform.GetChild(chunk_number).GetComponent<GenerateChunk>().World_GameObjects[local_position_x, (int)place.y]);
        Chunks.transform.GetChild(chunk_number).GetComponent<GenerateChunk>().World_GameObjects[local_position_x, (int)place.y] = null;


        /*
        int local_position_x = (int)place.x - ((int)chunk_width * chunk_number);
        int local_position_y = (int)place.y - ((int)chunk_width * chunk_number);
        int tile_hierarchy_number = (Chunks.transform.GetChild(0).transform.GetComponent<GenerateChunk>().height * local_position_x) + (int)place.y;
        global_tile_hierarchy_number = tile_hierarchy_number;
        Destroy(Chunks.transform.GetChild(chunk_number).GetChild(tile_hierarchy_number).gameObject);
        */
    }


    public bool IsOre (int id)
    {
        if (id >= 500)
            return true;
        else
            return false;
    }
}
