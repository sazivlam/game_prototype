﻿using UnityEngine;

[CreateAssetMenu]
public class ItemNew : ScriptableObject
{
    public string ItemName;
    public Sprite Icon;
}
