﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Handler : MonoBehaviour {

    public GameObject MainPanel;
    public GameObject MenuPanel;
    //public GameObject furnacePanel;
    public GameObject mainPanel;
    public GameObject canvas;
    public GameObject Tooltip;
    public GameObject quickAccessPanel;
    public GameObject Inventory;

    public GameObject background;

    public CraftingSystem CraftingSystem;

    //Axes count
    public const int axes_count = 2;

    string[] axes;
    bool[] axes_inUse;
    GameObject[] objects;
    bool[] objects_showing;

    private void Start()
    {
        canvas = GameObject.Find("Canvas");

        mainPanel = canvas.transform.GetChild(1).gameObject;

        Inventory = GameObject.Find("Inventory");
        //Array of axes names
        axes = new string[axes_count] { "Inventory", "Menu/Close" };
        //Array of axes states
        axes_inUse = new bool[axes_count] { false, false };
        //Array of UI objects
        objects = new GameObject[axes_count] { MainPanel, MenuPanel };
        //Array of object states
        objects_showing = new bool[axes_count] { false, false };

        Tooltip = GameObject.Find("Tooltip");

        CraftingSystem = Inventory.GetComponent<CraftingSystem>();
    }

    void Update () {

        //Checks every axis for input
        for (int i = 0; i < axes_count; i++)
        {
            if (Input.GetAxisRaw(axes[i]) == 1 && axes_inUse[i] == false)
            {
                axes_inUse[i] = true;

                if (objects_showing[i] == false)
                    Open(i);
                else
                    Close(i);
            }
        }

        for (int i = 0; i < axes_count; i++)
        {
            //Checks if axis button was unpressed, and changes its state
            if (Input.GetAxisRaw(axes[i]) == 0 && axes_inUse[i] == true)
                axes_inUse[i] = false;
        }



    }

    //Checks if any of UI elements are showing
    public bool IsAnything_Showing()
    {
        for (int i = 0; i < axes_count; i++)
        {
            if (objects_showing[i] == true)
                return true;
        }

        return false;
    }


    //Opens Main Panel
    public void Open()
    {
        Open(0);
    }

    //Closes Main Panel
    public void Close()
    {
        Close(0);
    }

    //Closes all UI elements except one with specific index
    public void CloseAll(int index)
    {
        for (int i = 0; i < axes_count; i++)
        {
            if (i == index)
                continue;

            Close(i);
        }

    }


    //Opens UI element at specific index
    public void Open(int index)
    {
        if (objects[index].name == "Menu Panel")
        {
            quickAccessPanel.SetActive(false);
            CloseAll(index);
        }

        if(objects_showing[1] == false)
        {
            background.SetActive(true);
            objects[index].SetActive(true);
            objects_showing[index] = true;
        }

        if (objects[index].name == "Main Panel")
        {
            quickAccessPanel.GetComponent<GraphicRaycaster>().enabled = true;
            //CraftingSystem.PrintOut();
            CraftingSystem.CheckRecipes();
            //CraftingSystem.CheckCraft();
        }

    }

    //Opens UI element at specific index
    public void Close(int index)
    {

        objects[index].SetActive(false);
        Tooltip.SetActive(false);
        objects_showing[index] = false;

        if (!IsAnything_Showing())
            background.SetActive(false);



        if (objects[index].name == "Main Panel")
        {
            //If furnace was active
            if (mainPanel.transform.GetChild(1).gameObject.activeSelf)
            {
                //Start cleanup from current active furnace
                mainPanel.transform.GetChild(1).GetChild(2).GetChild(0).GetChild(0).GetComponent<SpecificInputSlot>().furnace.cleanup();
                //Close furnace panel
                mainPanel.transform.GetChild(1).gameObject.SetActive(false);
            }
            //Open crafting panel
            mainPanel.transform.GetChild(0).gameObject.SetActive(true);
            quickAccessPanel.GetComponent<GraphicRaycaster>().enabled = false;
            CraftingSystem.ResetLastCraftedItemIndex();
        }

        if (objects[index].name == "Menu Panel")
            quickAccessPanel.SetActive(true);

    }


}
